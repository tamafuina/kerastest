﻿using DeepLearningChart.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeepLearningChart.Controllers
{
    public interface IMainWindowController:IViewModelController<MainWindowViewModel>
    {
        BaseChartViewModel CreateBaseChartViewModel();
        void OpenWindow();
        bool CanOpenWindow();
    }
}
