﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeepLearningChart.Controllers
{
    public interface IClassificationChartController:IBaseChartController
    {
        void LaunchClassification();
    }
}
