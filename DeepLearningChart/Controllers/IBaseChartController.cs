﻿using DeepLearningChart.ViewModel;
using LiveCharts;
using LiveCharts.Definitions.Series;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace DeepLearningChart.Controllers
{
    public interface IBaseChartController:IViewModelController<BaseChartViewModel>
    {
        void AddSeries(SeriesType seriesType, string seriesName, Color seriesColor);
        void AddChartPoints(string seriesName, double[] xPoints, double[] yPoints);
        void AddLiveChartPoint(string seriesName, double xPoint, double yPoint);
        void Timer();

        void AddAxisX(string axisName, AxisPosition position, IList<string>labels, double labelRotation);
        void AddAxisX(string axisName, AxisPosition position, double labelRotation);
        void AddAxisY(string axisName, AxisPosition position, double labelRotation);
    }
}
