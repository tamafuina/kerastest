﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeepLearningChart.Controllers
{
    public interface IViewModelController<T> where T : ViewModelBase
    {
        void SetViewModel(T viewModel);
    }
}
