﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using DeepLearningChart.Factories;
using DeepLearningChart.ViewModel;
using Unity;

namespace DeepLearningChart.Controllers
{
    public class MainWindowController : IMainWindowController
    {
        private readonly IViewModelFactory _Factory;
        private MainWindowViewModel _ViewModel;
        private readonly IUnityContainer _Container;

        #region Constructor
        public MainWindowController(IViewModelFactory factory, IUnityContainer container)
        {
            _Factory = factory ?? throw new ArgumentNullException(nameof(factory));
            _Container = container ?? throw new ArgumentNullException(nameof(container));
        }
        #endregion
        public BaseChartViewModel CreateBaseChartViewModel()
        {
            return _Factory.CreateBaseChartViewModel();
        }

        public void OpenWindow()
        {
            var viewModel = _Factory.CreateClassificationChartViewModel();
            var view = _Container.Resolve<ClassificationChart>();
            view.DataContext = viewModel;
            var window = new Window
            {
                Content = view
            };
            window.ShowDialog();
        }

        public void SetViewModel(MainWindowViewModel viewModel)
        {
            _ViewModel = viewModel ?? throw new ArgumentNullException(nameof(viewModel));
        }

        public bool CanOpenWindow()
        {
            return true;
        }
    }
}
