﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CNTKTest.Classification;
using DeepLearningChart.Factories;
using DeepLearningChart.ViewModel;

namespace DeepLearningChart.Controllers
{
    public class ClassificationChartController : BaseChartController, IClassificationChartController
    {
        #region fields
        ObservableCollection<double> _LossData;
        ObservableCollection<double> _AverageData;
        ClassificationChartViewModel _ViewModel;
        #endregion

        #region Constructor
        public ClassificationChartController(ISeriesFactory seriesFactory, IAxisFactory axisFactory) : base(seriesFactory, axisFactory)
        {
        }

        #endregion

        #region Methods
        public void LaunchClassification()
        {
            _LossData = new ObservableCollection<double>();
            _AverageData = new ObservableCollection<double>();
            _LossData.CollectionChanged += HandleLossDataCollectionChanged;
            _AverageData.CollectionChanged += HandleAverageDataCollectionChanged;
            var classification = new IrisClassification();
            var start = new ThreadStart(()=>classification.Train(_LossData, _AverageData));
            var newThread = new Thread(start);
            _ViewModel.ChartVisibility = true;
            newThread.Start();
            //do { _ViewModel.ChartVisibility = true; }
            //while (newThread.IsAlive);
            //newThread.Interrupt();
        }

        public override void SetViewModel(BaseChartViewModel viewModel)
        {
            base.SetViewModel(viewModel);
            _ViewModel = (ClassificationChartViewModel) viewModel;
        }
        
        #endregion

        #region Event Handlers
        private void HandleAverageDataCollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.Action != System.Collections.Specialized.NotifyCollectionChangedAction.Add) return;
            
            AddLiveChartPoint(_ViewModel.AverageSerie, _AverageData.Count - 1, e.NewItems.OfType<double>().ElementAt(0));
        }

        private void HandleLossDataCollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.Action != System.Collections.Specialized.NotifyCollectionChangedAction.Add) return;
            AddLiveChartPoint(_ViewModel.LossSerie, _LossData.Count - 1, e.NewItems.OfType<double>().ElementAt(0));
        }

        #endregion
    }
}
