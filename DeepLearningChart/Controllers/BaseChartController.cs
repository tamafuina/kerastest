﻿using DeepLearningChart.Factories;
using DeepLearningChart.ViewModel;
using LiveCharts;
using LiveCharts.Defaults;
using LiveCharts.Definitions.Series;
using LiveCharts.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Media;
using System.Windows.Threading;

namespace DeepLearningChart.Controllers
{
    public abstract class BaseChartController : IBaseChartController
    {
        #region Fields
        private BaseChartViewModel _ViewModel;
        #endregion

        #region Properties
        public ISeriesFactory _SeriesFactory { get; }
        public IAxisFactory _AxisFactory { get; }

        #endregion

        #region Constructor
        public BaseChartController(ISeriesFactory seriesFactory, IAxisFactory axisFactory)
        {
            _SeriesFactory = seriesFactory?? throw new ArgumentNullException(nameof(seriesFactory));
            _AxisFactory = axisFactory ?? throw new ArgumentNullException(nameof(axisFactory));
        }

        #endregion

        #region Methods
        public void AddSeries(SeriesType seriesType, string seriesName, Color seriesColor)
        {
            if (string.IsNullOrEmpty(seriesName))throw new ArgumentException("message", nameof(seriesName));
            if (_ViewModel.SeriesCollection == null)
                _ViewModel.SeriesCollection = new SeriesCollection();
            switch (seriesType)
            {
                case SeriesType.Line:
                    _ViewModel.SeriesCollection.Add( _SeriesFactory.CreateLineSeries(seriesName, seriesColor));
                    break;
                case SeriesType.Column:
                    _ViewModel.SeriesCollection.Add( _SeriesFactory.CreateColumnSeries(seriesName, seriesColor));
                    break;
                case SeriesType.Candle:
                    _ViewModel.SeriesCollection.Add( _SeriesFactory.CreateCandleSeries(seriesName, seriesColor));
                    break;
                case SeriesType.Pie:
                default:
                    _ViewModel.SeriesCollection.Add( _SeriesFactory.CreateLineSeries(seriesName, seriesColor));
                    break;
            }
        }

        public virtual void SetViewModel(BaseChartViewModel viewModel)
        {
            _ViewModel = viewModel ?? throw new ArgumentNullException(nameof(viewModel));
        }

        public virtual void AddChartPoints(string seriesName, double[] xPoints, double[] yPoints)
        {
            if (string.IsNullOrEmpty(seriesName))throw new ArgumentException("message", nameof(seriesName));
            if (xPoints == null) throw new ArgumentNullException(nameof(xPoints));
            if (yPoints == null)throw new ArgumentNullException(nameof(yPoints));
            if (xPoints.Length != yPoints.Length) throw new ArgumentOutOfRangeException($"{nameof(xPoints)} and {nameof(yPoints)} should have the same length");

            var series = _ViewModel.SeriesCollection.FirstOrDefault(s => ((Series)s).Name.Equals(seriesName)) ;
            var values = new ChartValues<ObservablePoint>();
            for (int index = 0; index < xPoints.Length; index++)
            {
                values.Add(new ObservablePoint(xPoints[index], yPoints[index]));
            }
            series.Values = values;
        }

        public virtual void AddLiveChartPoint(string seriesName, double xPoint, double yPoint)
        {
            if (string.IsNullOrEmpty(seriesName)) throw new ArgumentException("message", nameof(seriesName));
           
            Action<string, double, double> ActionAddPoint = (serieName,x,y) =>
            {
                var point = new ObservablePoint(x, y);
               Application.Current.Dispatcher.Invoke(new Action(() =>
                {
                    var series = _ViewModel.SeriesCollection.FirstOrDefault(s => ((Series)s).Name.Equals(seriesName));
                    if (series?.Values == null)
                        series.Values = new ChartValues<ObservablePoint>();
                    series.Values.Add(point);
                }),DispatcherPriority.ContextIdle);
            };

            try
            {
                var task = Task.Factory.StartNew(() => ActionAddPoint(seriesName, xPoint, yPoint));
            }
            catch { }
            
        }

        public void Timer()
        {
            var aTimer = new System.Timers.Timer();
            aTimer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
            aTimer.Interval = 500;
            aTimer.Enabled = true;

        }

        int count = 5;
        Random rd = new Random(1);
        private  void OnTimedEvent(object source, ElapsedEventArgs e)
        {
            AddLiveChartPoint("serieLine1", count++, rd.NextDouble() * 10);
            AddLiveChartPoint("serieColumn", count++, rd.NextDouble() * 10); 
        }

        public virtual void AddAxisX(string axisName, AxisPosition position, IList<string> labels, double labelRotation)
        {
            if (axisName == null) throw new ArgumentNullException(nameof(axisName));
            if (labels == null) throw new ArgumentNullException(nameof(labels));
            if (_ViewModel?.AxisXCollection == null)
                _ViewModel.AxisXCollection = new AxesCollection();
            _ViewModel.AxisXCollection.Add((Axis)_AxisFactory.CreateAxis(axisName, position, labels, labelRotation));
        }

        public virtual void AddAxisX(string axisName, AxisPosition position,  double labelRotation)
        {
            if (axisName == null) throw new ArgumentNullException(nameof(axisName));
            if (_ViewModel?.AxisXCollection == null)
                _ViewModel.AxisXCollection = new AxesCollection();
            _ViewModel.AxisXCollection.Add((Axis)_AxisFactory.CreateAxis(axisName, position, labelRotation));
        }

        public virtual void AddAxisY(string axisName, AxisPosition position,  double labelRotation)
        {
            if (axisName == null) throw new ArgumentNullException(nameof(axisName));
            if (_ViewModel?.AxisYCollection == null)
                _ViewModel.AxisYCollection = new AxesCollection();
            _ViewModel.AxisYCollection.Add((Axis)_AxisFactory.CreateAxis(axisName, position, labelRotation));
        }

        

        #endregion
    }
}
