﻿using DeepLearningChart.Controllers;
using DeepLearningChart.Factories;
using DeepLearningChart.ViewModel;
using GalaSoft.MvvmLight.Ioc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unity;
using Unity.Lifetime;

namespace DeepLearningChart
{
    public class ContainerMagic
    {
        public static void RegisterElements(IUnityContainer container)
        {
            //container.RegisterType<IUnityContainer, UnityContainer>(lifetimeManager:new ContainerControlledLifetimeManager() ); 
            container.RegisterType<MainWindowViewModel>();
            //container.RegisterType<BaseChartViewModel>();
            container.RegisterType<ClassificationChartViewModel>();
            //container.RegisterType<IBaseChartController, BaseChartController>();
            
            container.RegisterType<ISeriesFactory, SeriesFactory>();
            container.RegisterType<IViewModelFactory, ViewModelFactory>();
            container.RegisterType<IAxisFactory, AxisFactory>();
            container.RegisterType<IClassificationChartController, ClassificationChartController>();
            container.RegisterType<IMainWindowController, MainWindowController>();
            container.RegisterType(typeof(object), typeof(MainWindow), nameof(MainWindow));
            container.RegisterType(typeof(object), typeof(BaseChart), nameof(BaseChart));
            container.RegisterType(typeof(object), typeof(ClassificationChart), nameof(ClassificationChart));
            

        }
    }
}
