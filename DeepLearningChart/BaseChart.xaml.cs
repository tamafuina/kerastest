﻿using DeepLearningChart.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Unity.Attributes;

namespace DeepLearningChart
{
    /// <summary>
    /// Logique d'interaction pour BaseChart.xaml
    /// </summary>
    public partial class BaseChart : UserControl
    {
        [Dependency]
        public BaseChartViewModel ViewModel
        {
            set => DataContext = value;
        }
        public BaseChart()
        {
            InitializeComponent();
        }
    }
}
