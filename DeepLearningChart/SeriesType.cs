﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeepLearningChart
{
    public enum SeriesType
    {
        Line,
        Column,
        Candle,
        Pie
    }
}
