﻿using LiveCharts.Definitions.Series;
using LiveCharts.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace DeepLearningChart.Factories
{
    public interface ISeriesFactory
    {
        ILineSeriesView CreateLineSeries(string seriesName, Color seriesColor);
        IColumnSeriesView CreateColumnSeries(string seriesName, Color seriesColor);
        IFinancialSeriesView CreateCandleSeries(string seriesName, Color seriesColor);
    }
}
