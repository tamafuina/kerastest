﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using LiveCharts.Definitions.Series;
using LiveCharts.Wpf;

namespace DeepLearningChart.Factories
{
    public class SeriesFactory : ISeriesFactory
    {
        #region Methods
        public IColumnSeriesView CreateColumnSeries(string seriesName, System.Windows.Media.Color seriesColor)
        {
            if (string.IsNullOrEmpty(seriesName))throw new ArgumentException("message", nameof(seriesName));

            var series = new ColumnSeries();
            SetSeriesBasicProperties(series, seriesName, seriesColor);
            return series;
        }

        public IFinancialSeriesView CreateCandleSeries(string seriesName, System.Windows.Media.Color seriesColor)
        {
            var series = new CandleSeries();
            SetSeriesBasicProperties(series, seriesName, seriesColor);
            return series;
        }

        public ILineSeriesView CreateLineSeries(string seriesName, System.Windows.Media.Color seriesColor)
        {
            var series = new LineSeries();
            SetSeriesBasicProperties(series, seriesName, seriesColor);
            return series;
        }

        private void SetSeriesBasicProperties(Series series, string seriesName, System.Windows.Media.Color seriesColor)
        {
            if (series == null)throw new ArgumentNullException(nameof(series));
            if (string.IsNullOrEmpty(seriesName)) throw new ArgumentException("message", nameof(seriesName));
            series.Name = seriesName;
            series.Fill = new SolidColorBrush(Colors.Transparent);
            series.Stroke = new SolidColorBrush(seriesColor);
        }

        #endregion
    }
}
