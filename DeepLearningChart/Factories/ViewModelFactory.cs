﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DeepLearningChart.ViewModel;
using Unity;

namespace DeepLearningChart.Factories
{
    public class ViewModelFactory : IViewModelFactory
    {
        #region Fields
        private IUnityContainer _Container { get; }
        #endregion
        #region Constructor
        public ViewModelFactory(IUnityContainer container)
        {
            _Container = container ?? throw new ArgumentNullException(nameof(container));
        }

       
        #endregion
        public BaseChartViewModel CreateBaseChartViewModel()
        {
            return _Container.Resolve<BaseChartViewModel>();
        }

        public ClassificationChartViewModel CreateClassificationChartViewModel()
        {
            return _Container.Resolve<ClassificationChartViewModel>();
        }
    }
}
