﻿using LiveCharts;
using LiveCharts.Definitions.Charts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeepLearningChart.Factories
{
    public interface IAxisFactory
    {
        IAxisView CreateAxis(string axisName, AxisPosition position, IList<string> labels, double labelsRotation);
        IAxisView CreateAxis(string axisName, AxisPosition position, double labelsRotation);
    }
}
