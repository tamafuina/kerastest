﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LiveCharts;
using LiveCharts.Definitions.Charts;
using LiveCharts.Wpf;

namespace DeepLearningChart.Factories
{
    public class AxisFactory : IAxisFactory
    {
        public IAxisView CreateAxis(string axisName,AxisPosition position, IList<string> labels,double labelsRotation)
        {
            if (axisName == null)  throw new ArgumentNullException(nameof(axisName));
            if (labels == null) throw new ArgumentNullException(nameof(labels));

            var axis = new Axis()
            {
                Name = axisName,
                Position = position,
                Labels = labels,
                LabelsRotation= labelsRotation
            };
            return axis;
        }

        public IAxisView CreateAxis(string axisName, AxisPosition position, double labelsRotation)
        {
            if (axisName == null) throw new ArgumentNullException(nameof(axisName));

            var axis = new Axis()
            {
                Name = axisName,
                Position = position,
                LabelsRotation = labelsRotation
            };
            return axis;
        }
    }
}
