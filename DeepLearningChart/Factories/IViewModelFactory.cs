﻿using DeepLearningChart.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeepLearningChart.Factories
{
    public interface IViewModelFactory
    {
        BaseChartViewModel CreateBaseChartViewModel();
        ClassificationChartViewModel CreateClassificationChartViewModel();
    }
}
