﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media;
using DeepLearningChart.Controllers;
using GalaSoft.MvvmLight.Command;

namespace DeepLearningChart.ViewModel
{
    public class ClassificationChartViewModel : BaseChartViewModel
    {

        private readonly IClassificationChartController _Controller;
        private bool _ChartVisibility=true;

        #region Properties
        public string LossSerie => "Loss";
        public string AverageSerie => "Average";

        public ICommand LaunchClassificationCommand { get; }
        public bool ChartVisibility
        {
            get => _ChartVisibility;
            set
            {
                if (_ChartVisibility == value) return;
                _ChartVisibility = value;
            }
        }

        #endregion

        public ClassificationChartViewModel(IClassificationChartController controller) : base(controller)
        {
            _Controller = controller ?? throw new ArgumentNullException(nameof(controller));
            LaunchClassificationCommand = new RelayCommand(controller.LaunchClassification);
            controller.SetViewModel(this);

            controller.AddAxisX("Iteration", LiveCharts.AxisPosition.LeftBottom, 0);
            controller.AddAxisY("Proportion", LiveCharts.AxisPosition.LeftBottom, 0);
            controller.AddSeries(SeriesType.Line, LossSerie, Colors.Blue);
            controller.AddSeries(SeriesType.Line, AverageSerie, Colors.Red);
        }
    }
}
