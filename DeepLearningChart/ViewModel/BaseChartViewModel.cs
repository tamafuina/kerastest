﻿using DeepLearningChart.Controllers;
using GalaSoft.MvvmLight;
using LiveCharts;
using LiveCharts.Defaults;
using LiveCharts.Wpf;
using System.Collections.Generic;
using System.Windows.Media;

namespace DeepLearningChart.ViewModel
{
    public abstract class BaseChartViewModel : ViewModelBase
    {
        #region Fields

        SeriesCollection _SeriesCollection;
        string _Title = "First Line chart on LiveChart";
        private AxesCollection _AxisXCollection;
        private AxesCollection _AxisYCollection;
        private readonly IBaseChartController _Controller;

        #endregion

        #region Properties
        public SeriesCollection SeriesCollection
        {
            get => _SeriesCollection;
            set
            {
                if (_SeriesCollection == value) return;
                _SeriesCollection = value;
                RaisePropertyChanged(nameof(SeriesCollection));
            }
        }
        public string Title
        {
            get => _Title;
            set
            {
                if (_Title == value) return;
                _Title = value;
                RaisePropertyChanged(nameof(Title));
            }
        }

        public AxesCollection AxisXCollection
        {
            get => _AxisXCollection;
            set
            {
                if (_AxisXCollection == value) return;
                _AxisXCollection = value;
                RaisePropertyChanged(nameof(AxisXCollection));
            }
        }

        public AxesCollection AxisYCollection
        {
            get => _AxisYCollection;
            set
            {
                if (_AxisYCollection == value) return;
                _AxisYCollection = value;
                RaisePropertyChanged(nameof(AxisYCollection));
            }
        }

        #endregion

        #region Constructor
        public BaseChartViewModel(IBaseChartController controller)
        {
            //SeriesCollection = new SeriesCollection()
            //{
            //    new LineSeries
            //    {
            //        Values =new ChartValues<ObservablePoint>
            //        {
            //            new ObservablePoint(1,2),
            //            new ObservablePoint(2.5d,1),
            //            new ObservablePoint(4,25),
            //            new ObservablePoint(5,-2d)
            //        }

            //    }
            //};
            _Controller = controller ?? throw new System.ArgumentNullException(nameof(controller));
            _Controller.SetViewModel(this);

            //var seriesName = "serieLine1";
            //var seriesName1 = "serieColumn";
            //_Controller.AddAxisX("axisX1", AxisPosition.LeftBottom, new List<string>() { "Moi", "Toi", "Lui", "Nous" }, 45);
            //_Controller.AddAxisY("axisY1", AxisPosition.RightTop, 0);
            //_Controller.AddSeries(SeriesType.Line, seriesName, Colors.Transparent);
            ////_Controller.AddSeries(SeriesType.Column, seriesName1, Colors.Red);
            //_Controller.AddChartPoints(seriesName, new[] { 0, 1, 2, 3d }, new[] { 2, 1, 25, -2d });
            ////_Controller.AddChartPoints(seriesName1, new[] { 1, 2.5, 4, 5 }, new[] { 2, 1, 25, -2d });
            ////_Controller.Timer();
        }

        #endregion
    }
}
