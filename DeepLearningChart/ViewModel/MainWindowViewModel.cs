﻿using DeepLearningChart.Controllers;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace DeepLearningChart.ViewModel
{
    public class MainWindowViewModel:ViewModelBase
    {
        private string _Title="Chart Example";
        private readonly IMainWindowController _Controller;

        #region Properties
        public string Title
        {
            get => _Title;
            set
            {
                if (_Title == value) return;
                _Title = value;
                RaisePropertyChanged(nameof(Title));
            }
        }
        public BaseChartViewModel BaseChartVM { get; }
        public ICommand OpenWindowCommand { get; set; }

        #endregion
        public MainWindowViewModel(IMainWindowController controller)
        {
           
           _Controller = controller ?? throw new ArgumentNullException(nameof(controller));
            controller.SetViewModel(this); OpenWindowCommand = new RelayCommand(()=>_Controller.OpenWindow(),()=>CanOpen());
            
            //BaseChartVM = controller.CreateBaseChartViewModel();
            //controller.OpenWindow();
        }

        private bool CanOpen() => _Controller.CanOpenWindow();
        private void OpenWindow() => _Controller.OpenWindow();
     
    }
}
