﻿using CommonServiceLocator;
using DeepLearningChart.ViewModel;
using GalaSoft.MvvmLight.Ioc;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Unity;
using Unity.ServiceLocation;

namespace DeepLearningChart
{
    /// <summary>
    /// Logique d'interaction pour App.xaml
    /// </summary>
    public partial class App : Application
    {
        IUnityContainer _Container;
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            _Container = new UnityContainer();
            ContainerMagic.RegisterElements(_Container);
            UnityServiceLocator locator = new UnityServiceLocator(_Container);
            ServiceLocator.SetLocatorProvider(() => locator);
            //ServiceLocator.Current.GetInstance<MainWindowViewModel>();
           // ServiceLocator.Current.GetInstance<BaseChartViewModel>();
            var main = _Container.Resolve<MainWindow>();
            //container.Resolve<BaseChart>();
            main.Show();
        }
    }
}
