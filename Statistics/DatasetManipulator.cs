﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Statistics
{
    public static class DatasetManipulator
    {
        /// <summary>
        /// https://forum.unity.com/threads/randomize-array-in-c.86871/
        /// </summary>
        /// <param name="input"></param>
        public static void Shuffle<T>(T[]input)
        {
            var array = new T[input.Length];
            var random = new Random();
            int index;
            for(var i=0;i<input.Length;i++)
            {
                index =random.Next(i, input.Length);
                var temp = input[index];
                input[index] = input[i];
                input[i] = temp;
            }

        }

        public static void Shuffle<T>(T[,] input)
        {
            var array = new T[input.GetLength(0),input.GetLength(1)];
            var random = new Random();
            int index;
            for (var i = 0; i < input.GetLength(0); i++)
            {
                index = random.Next(i, input.GetLength(0));
                var temp = new T[input.GetLength(1)];
                for (var j=0;j<input.GetLength(1);j++)
                {
                    temp[j] = input[index, j];
                }
                for (var j = 0; j < input.GetLength(1); j++)
                {
                    input[index,j] = input[i, j];
                    input[i, j] = temp[j];
                }
            }

        }
        public static IList<int> Shuffle(int size)
        {
            var array = new int[size];
            for (var i = 0; i < size; i++)
                array[i]=i;
            Shuffle(array);
            return array;
        }
        public static void SplitDataset<T>(T[,] input,out T [,] training, out T[,] testing, double percentageTesting=0.2, bool randomizedsplit=false)
        {
            if (input == null) throw new ArgumentNullException(nameof(input));
            if (percentageTesting < 0 && percentageTesting > 1) throw new IndexOutOfRangeException($"{nameof(percentageTesting)} should be between 0 and 1");
            var testingRange = (int)(1 / percentageTesting);
            testing = new T[(int)Math.Ceiling((input.GetLength(0) * percentageTesting)), input.GetLength(1)];
            training = new T[input.GetLength(0) - testing.GetLength(0), input.GetLength(1)];
            var trainingIndex = 0;
            var testingIndex = 0;
            if (randomizedsplit)
                Shuffle(input);
            for(var i=0;i<input.GetLength(0);i++)
            {
                if(i%testingRange==0)
                {
                    for (var j = 0; j < input.GetLength(1); j++)
                        testing[testingIndex, j] = input[i, j];
                    testingIndex++;
                }
                else
                {
                    for (var j = 0; j < input.GetLength(1); j++)
                        training[trainingIndex, j] = input[i, j];
                    trainingIndex++;
                }
            }
        }
        public static void SplitDataset<T>(T[]input, out T[]training, out T[]testing, double percentageTesting = 0.2, bool randomizedsplit = false)
        {
            if (input == null) throw new ArgumentNullException(nameof(input));
            if (percentageTesting < 0 && percentageTesting > 1) throw new IndexOutOfRangeException($"{nameof(percentageTesting)} should be between 0 and 1");
            var testingRange = (int)(1 / percentageTesting);
            testing = new T[(int)(input.GetLength(0) * percentageTesting)];
            training = new T[input.GetLength(0) - testing.GetLength(0)];
            var trainingIndex = 0;
            var testingIndex = 0;
            if (randomizedsplit)
                Shuffle(input);
            for (var i = 0; i < input.GetLength(0); i++)
            {
                if (i % testingRange == 0)
                {
                    testing[testingIndex] = input[i];
                    testingIndex++;
                }
                else
                {
                    training[trainingIndex] = input[i];
                    trainingIndex++;
                }
                        
            }
        }
    }
}
