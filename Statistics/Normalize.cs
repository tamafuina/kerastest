﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Statistics
{
    public static class Normalize
    {
        public static float[][] Normalized(float[][] array, float[] means, float[] standardDeviations)
        {
            if (array == null) throw new ArgumentNullException(nameof(array));
            if (means == null)
                throw new ArgumentNullException(nameof(means));
            if (standardDeviations == null)
                throw new ArgumentNullException(nameof(standardDeviations));
            var normalized = new float[array.Length][];
            for (var i = 0; i < array.Length; i++)
            {
                var vector = new float[array[i].Length];
                for (var j = 0; j < array[i].Length; j++)
                    vector[j] = (array[i][j] - means[j]) / standardDeviations[j];
                normalized[i] = vector;
            }
            return normalized;

        }

        public static double[][] Normalized(double[][]array, double[]means, double[] standardDeviations)
        {
            if (array == null) throw new ArgumentNullException(nameof(array));
            if (means == null)
                throw new ArgumentNullException(nameof(means));
            if (standardDeviations == null)
                throw new ArgumentNullException(nameof(standardDeviations));
            var normalized = new double[array.Length][];
            for(var i=0;i<array.Length;i++)
            {
                var vector = new double[array[i].Length];
                for (var j = 0; j < array[i].Length; j++)
                    vector[j] = (array[i][j] - means[j]) / standardDeviations[j];
                normalized[i] = vector;
            }
            return normalized;

        }
        public static float[][] UnNormalized(float[][] array, float[] means, float[] standardDeviations)
        {
            if (array == null) throw new ArgumentNullException(nameof(array));
            if (means == null)
                throw new ArgumentNullException(nameof(means));
            if (standardDeviations == null)
                throw new ArgumentNullException(nameof(standardDeviations));
            var unNormalized = new float[array.Length][];
            for (var i = 0; i < array.Length; i++)
            {
                var vector = new float[array[i].Length];
                for (var j = 0; j < array.Length; j++)
                    vector[j] = UnNormalized(array[i][j], means[j], standardDeviations[j]);
                unNormalized[i] = vector;
            }
            return unNormalized;

        }
        public static double[][] UnNormalized(double[][] array, double[] means, double[] standardDeviations)
        {
            if (array == null) throw new ArgumentNullException(nameof(array));
            if (means == null)
                throw new ArgumentNullException(nameof(means));
            if (standardDeviations == null)
                throw new ArgumentNullException(nameof(standardDeviations));
            var unNormalized = new double[array.Length][];
            for (var i = 0; i < array.Length; i++)
            {
                var vector = new double[array[i].Length];
                for (var j = 0; j < array.Length; j++)
                    vector[j] = UnNormalized(array[i][j], means[j], standardDeviations[j]);

                unNormalized[i] = vector;
            }
            return unNormalized;

        }

        public static float UnNormalized(float normalizedValue,float mean, float std)
        {            
            return (normalizedValue * std) + mean;
        }
        public static double UnNormalized(double normalizedValue, double mean, double std)
        {
            UnNormalized((float)normalizedValue, (float)mean, (float)std);
            return (normalizedValue * std) + mean;
        }

        public static void Normalized(double[] array, double means, double standardDeviations, double[] vector)
        {
            for (var j = 0; j < array.Length; j++)
                vector[j] = (array[j] - means) / standardDeviations;
        }
    }
}
