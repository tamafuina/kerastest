﻿using Accord.Statistics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Statistics
{
    public class Statistics
    {
        public static float[] Average(float[][] array)
        {
            if (array == null) throw new ArgumentNullException(nameof(array));
            var average = new float[array[0].Length];
            for (var i = 0; i < array[0].Length; i++)
                average[i] = Average(ArrayHelpers.ArrayToVector(array, i));
            return average;
        }
        public static double[] Average(double[][] array)
        {
            if (array == null) throw new ArgumentNullException(nameof(array));
            var average = new double[array[0].Length];
            for (var i = 0; i < array[0].Length; i++)
                average[i] = Average(ArrayHelpers.ArrayToVector(array, i));
            return average;
        }

        public static float[] Average(float[,] array)
        {
            if (array == null) throw new ArgumentNullException(nameof(array));
            var average = new float[array.GetLength(1)];
            for (var i = 0; i < array.GetLength(1); i++)
                average[i] = Average(ArrayHelpers.ArrayToVector(array, i));
            return average;

        }
        public static double[] Average(double[,] array)
        {
            if (array == null) throw new ArgumentNullException(nameof(array));
            var average = new double[array.GetLength(1)];
            for (var i = 0; i < array.GetLength(1); i++)
                average[i] = Average(ArrayHelpers.ArrayToVector(array, i));
            return average;

        }

        public static float Average(IEnumerable<float> array)
        {
            if (array == null) throw new ArgumentNullException(nameof(array));
            return array.Average();
        }
        public static double Average(IEnumerable<double> array)
        {
            if (array == null) throw new ArgumentNullException(nameof(array));
            return array.Average();
        }
        public static float[] StandardDeviation(float[][] array)
        {
            if (array == null) throw new ArgumentNullException(nameof(array));
            var std = new float[array[0].Length];
            for (var i = 0; i < array[0].Length; i++)
                std[i] = StandardDeviation(ArrayHelpers.ArrayToVector(array, i));
            return std;
        }
        public static double[] StandardDeviation(double[][] array)
        {
            if (array == null) throw new ArgumentNullException(nameof(array));
            var std = new double[array[0].Length];
            for (var i = 0; i < array[0].Length; i++)
                std[i] = StandardDeviation(ArrayHelpers.ArrayToVector(array, i));
            return std;
        }

        public static float[] StandardDeviation(float[,] array)
        {
            if (array == null) throw new ArgumentNullException(nameof(array));
            var std = new float[array.GetLength(1)];
            for (var i = 0; i < array.GetLength(1); i++)
                std[i] = StandardDeviation(ArrayHelpers.ArrayToVector(array, i));
            return std;

        }
        public static double[] StandardDeviation(double[,] array)
        {
            if (array == null) throw new ArgumentNullException(nameof(array));
            var std = new double[array.GetLength(1)];
            for (var i = 0; i < array.GetLength(1); i++)
                std[i] = StandardDeviation(ArrayHelpers.ArrayToVector(array, i));
            return std;

        }

        public static float StandardDeviation(IEnumerable<float> array)
        {
            if (array == null) throw new ArgumentNullException(nameof(array));
            return (float)Measures.StandardDeviation(array.ToArray());
        }
        public static double StandardDeviation(IEnumerable<double>array)
        {
            if (array == null) throw new ArgumentNullException(nameof(array));
            return Measures.StandardDeviation(array.ToArray());
        }


    }
}
