﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Statistics
{
    public class DatasetFilter
    {
        public static T[,] RemoveNaN<T>(T[,]data)
        {
            var indexes = NaNIndex(data);
            var newData = new T[data.GetLength(0) - indexes.Count, data.GetLength(1)];
            var index = 0;
            for(var i=0;i<data.GetLength(0);i++)
            {
                if(indexes.BinarySearch(i)<0)
                {
                    for (var j = 0; j < data.GetLength(1); j++)
                        newData[index, j] = data[i, j];
                    index++;
                }
            }
            return newData;
        }

        private static  List<int> NaNIndex<T>(T[,]data)
        {
            if (typeof(T) != typeof(double) && typeof(T) != typeof(float) && typeof(T) != typeof(int)) throw new InvalidCastException(nameof(data));
            if (data == null) throw new ArgumentNullException(nameof(data));
            var indexes = new List<int>();
            for (var i = 0; i < data.GetLength(0); i++)
                for (var j = 0; j < data.GetLength(1); j++)
                {
                    var doubleNumber = (double)(object)data[i, j];
                    if (double.IsNaN(doubleNumber))
                    {
                        indexes.Add(i);
                        break;
                    }
                }
            return indexes;
        }
    }
}
