﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Statistics
{
    public class StatisticsEstimator
    {
        public static double Mse<T>(T[] predicted, T[] actual)
        {
            if (typeof(T) == typeof(double) || typeof(T) == typeof(int) || typeof(T) == typeof(Int16) || typeof(T) == typeof(Int32) || typeof(T) == typeof(Int64) || typeof(T) == typeof(float)) ;
            return Mse(ConvertToDoubleArray(predicted), ConvertToDoubleArray(actual));
        }
        public static double Mse(double[] predicted, double[] actual)
        {
            if (predicted == null)
                throw new ArgumentNullException(nameof(predicted));
            if (actual == null)
                throw new ArgumentNullException(nameof(actual));
            if (predicted.Length != actual.Length) throw new ArgumentException();
            var average = Statistics.Average(actual);
            
            var sum = 0d;
            for (var i = 0; i < predicted.Length; i++)
            {
                sum += Math.Pow((predicted[i] - average), 2);
            }
            return sum / predicted.Length;
        }

        public static double R2<T>(T[]predicted, T[]actual)
        {
            return R2(ConvertToDoubleArray(predicted), ConvertToDoubleArray(actual));
        }
        public static double R2(double[] predicted, double[] actual)
        {
            if (predicted == null)
                throw new ArgumentNullException(nameof(predicted));
            if (actual == null)
                throw new ArgumentNullException(nameof(actual));
            var sse = 0d;
            var sso = 0d;
            var predictedAverage = Statistics.Average(predicted);
            var actualAverage = Statistics.Average(actual);
            for(var i=0;i<predicted.Length;i++)
            {
                sse += Math.Pow((predicted[i] - predictedAverage), 2);
                sso += Math.Pow((actual[i] - actualAverage), 2);
            }
            return sse / sso;
        }

        private static double[] ConvertToDoubleArray<T>(T[]array)
        {
            if (array == null)
                throw new ArgumentNullException(nameof(array));
            var doubleArray = new double[array.Length];
            for(var i=0;i<array.Length;i++)
            {
                doubleArray[i] = Convert.ToDouble(array[i].ToString());
            }
            return doubleArray;
        }
    }
}
