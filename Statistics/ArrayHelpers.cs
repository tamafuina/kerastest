﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Statistics
{
    public static  class ArrayHelpers
    {
        public static IEnumerable<float> ArrayToVector(float[,] array, int columnIndex)
        {
            if (array == null) throw new ArgumentNullException(nameof(array));
            if (array.GetLength(1) <= columnIndex) throw new IndexOutOfRangeException(nameof(array));
            var vector = new float[array.GetLength(0)];
            for (var i = 0; i < array.GetLength(0); i++)
                vector[i] = array[i, columnIndex];
            return vector;
        }
        public static IEnumerable<double> ArrayToVector(double[,] array, int columnIndex) 
        {
            if (array == null) throw new ArgumentNullException(nameof(array));
            if (array.GetLength(1) <= columnIndex) throw new IndexOutOfRangeException(nameof(array));
            var vector = new double[array.GetLength(0)];
            for (var i = 0; i < array.GetLength(0); i++)
                vector[i] = array[i, columnIndex];
            return vector;
        }
        public static IEnumerable<T> ArrayToVector<T>(T[][] array, int columnIndex)
        {
            if (array == null) throw new ArgumentNullException(nameof(array));
            if (array.GetLength(0) <= 0) throw new IndexOutOfRangeException(nameof(array));
            var vector = new T[array.Length];
            for (var i = 0; i < array.Length; i++)
            {
                if (array[i].Length <= columnIndex)
                    throw new IndexOutOfRangeException(nameof(array));
                vector[i] = array[i][columnIndex];
            }
            return vector;
        }
        public static IEnumerable<double> ArrayToVector(double[][] array, int columnIndex)
        {
            if (array == null) throw new ArgumentNullException(nameof(array));
            if (array.GetLength(0) <= 0) throw new IndexOutOfRangeException(nameof(array));
            var vector = new double[array.Length];
            for (var i = 0; i < array.Length; i++)
            {
                if (array[i].Length <= columnIndex)
                    throw new IndexOutOfRangeException(nameof(array));
                vector[i] = array[i][columnIndex];
            }
            return vector;
        }

        public static float[][] ListToArray(IList<IList<float>> array)
        {
            if (array == null)
                throw new ArgumentNullException(nameof(array));
            var newArray = new float[array.Count][];
            for(var i=0;i<array.Count;i++)
            {
                var items = new float[array[i].Count];
                for(var j=0;j<array[i].Count;j++)
                {
                    items[j] = array[i][j];
                }
                newArray[i] = items;
            }
            return newArray;
        }
    }
}
