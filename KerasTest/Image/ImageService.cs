﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KerasTest.Image
{
    public class ImageService
    {
        public static Bitmap Resize(Bitmap image,int width, int height)
        {
            return new Bitmap(image, width, height);
        }

        public static byte[] BitmapToArray(Bitmap image)
        {
            if (image == null)  throw new ArgumentNullException(nameof(image));
            var converter = new ImageConverter();

            return (byte[])converter.ConvertTo(image, typeof(byte[]));
        }

        public Bitmap ArrayToBitmap(byte[] imageData)
        {
            if (imageData == null)  throw new ArgumentNullException(nameof(imageData));
            Bitmap bmp = null;
            using (var stream = new MemoryStream(imageData))
            {
                bmp = new Bitmap(stream);
            }
            return bmp;
        }

        public static Bitmap RGBToGrayScale(Bitmap image)
        {
            if (image == null) throw new ArgumentNullException(nameof(image));

            var grayScale = new Bitmap(image.Width, image.Height);
            for (int x = 0; x < image.Width; x++)
            {
                for (int y = 0; y < image.Height; y++)
                {
                    var color = image.GetPixel(x, y);
                    var l = Convert.ToInt32(color.R * .2026 + color.G * .7152 + color.B * .0722);
                    grayScale.SetPixel(x, y, Color.FromArgb(l, l, l));
                }
            }
            return grayScale;
        }
    }
}
