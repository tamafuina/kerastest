﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KerasTest.LoadData
{
    public class LoadImageDataService
    {

        public static float[][] LoadBinaryFile(string filePath, int rowNumbers, int colNumbers)
        {
            var buffer = new byte[sizeof(float) * rowNumbers * colNumbers];
            using (var reader = new BinaryReader(File.OpenRead(filePath)))
            {
                reader.Read(buffer, 0, buffer.Length);
            }
            var dst = new float[rowNumbers][];
            for(var index=0;index<rowNumbers;index++)
            {
                dst[index] = new float[colNumbers];
                Buffer.BlockCopy(buffer, rowNumbers * colNumbers, dst[index], 0, colNumbers);
            }
            return dst;
        }
    }
}
