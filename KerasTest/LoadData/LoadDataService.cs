﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using SkiaSharp;
using System.Drawing;
using Accord.IO;
using System.Globalization;

namespace KerasTest.LoadData
{
    public class LoadDataService
    {
        const int size = 150;
        const int quality = 75;
        public IDictionary<string,Bitmap> LoadTrafficImage(string path)
        {
            var directories = Directory.GetDirectories(path);
            var files = new Dictionary<string, IEnumerable<string>>();
            foreach (var directory in directories)
                files.Add(directory, Directory.GetFiles(directory).Where(f => f.EndsWith(".ppm")));
            var images = new Dictionary<string,Bitmap>();
            foreach (var file in files.Keys)
                foreach (var f in files[file])
                {
                    var name = f.Split('\\');
                    images.Add($"{name[name.Length-2]}_{name[name.Length-1].Replace(".ppm","")}", ReadBitmapFromPPM(f)); }
            return images;
        }


        public static Bitmap ReadBitmapFromPPM(string file)
        {
            var reader = new BinaryReader(new FileStream(file, FileMode.Open));
            if (reader.ReadChar() != 'P' || reader.ReadChar() != '6')
                return null;
            reader.ReadChar(); //Eat newline
            string widths = "", heights = "";
            char temp;
            while ((temp = reader.ReadChar()) != ' ')
                widths += temp;
            while ((temp = reader.ReadChar()) >= '0' && temp <= '9')
                heights += temp;
            if (reader.ReadChar() != '2' || reader.ReadChar() != '5' || reader.ReadChar() != '5')
                return null;
            reader.ReadChar(); //Eat the last newline
            int width = int.Parse(widths),
                height = int.Parse(heights);
            Bitmap bitmap = new Bitmap(width, height);
            //Read in the pixels
            for (int y = 0; y < height; y++)
                for (int x = 0; x < width; x++)
                {
                    var color = Color.FromArgb(reader.ReadByte(), reader.ReadByte(), reader.ReadByte());
                    bitmap.SetPixel(x, y, color);
                }
            return bitmap;
        }

        private SKBitmap LoadImage(string path)
        {
            using (var input = File.OpenRead(path))
            {
                using (var inputStream = new SKManagedStream(input))
                {
                    using (var original = SKBitmap.Decode(inputStream))
                    {
                        int width, height;
                        if (original.Width > original.Height)
                        {
                            width = size;
                            height = original.Height * size / original.Width;
                        }
                        else
                        {
                            width = original.Width * size / original.Height;
                            height = size;
                        }
                        using (var resized = original.Resize(new SKImageInfo(width, height), SKBitmapResizeMethod.Lanczos3))
                        {
                            if (resized == null) return null;
                            return resized;
                        }
                    }
                }
            }
               
        }

        public static double[,] ReadCsvToMatrix(string fileName)
        {
            var csv = File.ReadAllText(fileName);            
            var reader = CsvReader.FromText(csv, false);
    
            
            var count = 0;
            var inputList = reader.ToList();
            var data = new double[inputList.Count, reader.FieldCount];

            foreach (var line in inputList)
            {                
                for (var index = 0; index < reader.FieldCount; index++)
                    data[count, index] = Convert.ToDouble(line[index], CultureInfo.GetCultureInfo("en-En").NumberFormat);
                count++;
            }

            return data;
        }
    }
}
