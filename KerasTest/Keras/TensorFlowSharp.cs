﻿using Accord.Math;
using KerasTest.Image;
using KerasTest.LoadData;
using SkiaSharp;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TensorFlow;

namespace KerasTest.Keras
{
    public class TensoFlowSharp
    {

        public void AddDouble()
        {
            using (var session = new TFSession())
            {
                var graph = session.Graph;

                var a = graph.Const(new TFTensor(new double[] { 1, 2, 3, 4 }));
                var b = graph.Const(new TFTensor(new double[] { 5, 6, 7, 8 }));
                Console.WriteLine("a=2 b=3");

                // Add two constants
                var addingResults = session.GetRunner().Run(graph.Add(a, b));
                var addingResultValue = addingResults.GetValue() as double[];
                Console.WriteLine("a+b={0}", string.Join(";", addingResultValue));

                // Multiply two constants
                var multiplyResults = session.GetRunner().Run(graph.Mul(a, b));
                var multiplyResultValue = multiplyResults.GetValue() as double[];
                Console.WriteLine("a*b={0}", string.Join(";", multiplyResultValue));
                Console.ReadLine();
            }
        }

        public void Multiply()
        {
            var number = 1_000_000;
            var array1 = new double[number];
            var array2 = new double[number];
            var array3 = new double[number];
            for (var index = 0; index < number; number++)
            {
                array1[index] = index;
                array2[index] = index;
            }

            var sw = new Stopwatch();
            sw.Start();

            for (var index = 0; index < number; number++)
                array3[index] = array1[index] * array2[index];
            sw.Stop();

            Console.WriteLine($"TIME classic multiplication : {sw.ElapsedMilliseconds}");

            using (var session = new TFSession())
            {
                var graph = session.Graph;

                var a = graph.Const(new TFTensor(array1));
                var b = graph.Const(new TFTensor(array2));
                Console.WriteLine("a=2 b=3");

                sw.Restart();
                // Multiply two constants
                var multiplyResults = graph.Mul(a, b);
                sw.Stop();
                Console.WriteLine($"Result :{multiplyResults}");
                Console.WriteLine($"TIME Tensorflow : {sw.ElapsedMilliseconds}");
                Console.ReadLine();
            }

        }

        public void ConfigDevice(IList<Bitmap> input)
        {
            input = Resize(input);

            var tfSessions = new TFSessionOptions();
            tfSessions.SetConfig(IntPtr.Zero, 1000, TFStatus.Default);
            var session = new TFSession();
            var graph = session.Graph;

            var x = graph.Placeholder(TFDataType.Float, new TFShape(28, 28));
            var y = graph.Placeholder(TFDataType.Int32, new TFShape(1));

            
        }

        public void DisplayImageProperties(Bitmap image)
        {
            var skImage = new SKBitmap();
        }

        private IList<Bitmap> Resize(IList<Bitmap> images)
        {
            var resizedImages = new List<Bitmap>();
            for (int index = 0; index < images.Count; index++)
            {
                resizedImages.Add(ImageService.Resize(images[index], 28, 28));
            }

            return resizedImages;
        }

        private double[,] CreatePatient(int rows, int cols, double[,] constant)
        {
            if (constant == null) throw new ArgumentNullException(nameof(constant));
            if (constant.GetLength(1) != cols) throw new ArgumentOutOfRangeException();

            var patient = new double[rows, cols];
            var rd = new Random(1);
            for (var rowIndex = 0; rowIndex < rows; rowIndex++)
            {
                for (var colIndex = 0; colIndex < cols; colIndex++)
                    patient[rowIndex, colIndex] = rd.NextDouble() + constant[0, colIndex];
            }

            return patient;
        }

        private int[] CreateTarget(int rows,int value)
        {
            var target = new int[rows];
            for (var index = 0; index < rows; index++)
                target[index] = value;
            return target;
        }

        private (double[] Weights,double Bias) InitializeVariable(int cols)
        {
            if (cols < 1) throw new ArgumentException();
            var weights = new double[cols];
            var rd = new Random(1);
            for (var index = 0; index < cols; index++)
                weights[index] = rd.NextDouble();
            return (Weights: weights, Bias: 0);
        }

        private double[] PreActivation(double[,]x, double[]w, double b)
        {
            if (x == null)  throw new ArgumentNullException(nameof(x));
            if (w == null) throw new ArgumentNullException(nameof(w));
            if (x.GetLength(1) != w.Length) throw new ArgumentOutOfRangeException();
            var z = new double[x.GetLength(0)];
            var a = x.Dot(w).Add(b);
            for (var rowIndex = 0; rowIndex < x.GetLength(0); rowIndex++)
            {
                for (var colIndex = 0; colIndex < x.GetLength(1); colIndex++)
                    z[rowIndex] += x[rowIndex, colIndex] * w[colIndex];
                z[rowIndex] += b;
            }
            var s = a.Subtract(z);
            return z;
            
        }

        private double[] Activation(double[]z)
        {
            if (z == null)throw new ArgumentNullException(nameof(z));
            var y = new double[z.Length];

            for (var index = 0; index < z.Length; index++)
                y[index] = 1 / (1 + Math.Exp(-(z[index])));
            return y;
        }

        private double[,] CreateData()
        {
            var size = 100;
            var random = new Random(1);
            var sick = CreatePatient(size, 2, new double[,] { { 2, 2 } });
           // var sick2 = new int[100, 2];
            var healthy = CreatePatient(size, 2, new double[,] { { -2, -2 } });
            //var healthy2 = new int[100, 2];
           

            var patients = new double[sick.GetLength(0)  + healthy.GetLength(0) , 2];
            for(var index=0;index<size;index++)
            {
                patients[index, 0] = sick[index, 0];
                patients[index, 1] = sick[index, 1];
                patients[index + size, 0] =healthy[index, 0];
                patients[index + size, 1] = healthy[index, 1];
                //patients[index + (size * 2), 0] = healthy[index, 0];
                //patients[index + (size * 2), 1] = healthy[index, 1];
                //patients[index + (size * 3), 0] = healthy[index, 0];
                //patients[index + (size * 3), 1] = healthy[index, 1];
            }
            return patients;
        }

        private double[] Predict(double[,] features, double[] weights, double bias)
        {
            if (features == null)  throw new ArgumentNullException(nameof(features));
            var z = PreActivation(features, weights, bias);
            var y = Activation(z);

            //var predict = new double[y.Length];
            //for (int index = 0; index < y.Length; index++)
            //{
            //    predict[index] = y[index]
            //}
            return y;
        }

        private double Accuracy(double[] predictions, int[] targets)
        {
            if (predictions == null) throw new ArgumentNullException(nameof(predictions));
            if (targets == null) throw new ArgumentNullException(nameof(targets));
            var vectorPrediction = new int[targets.Length];
            for(var index =0;index<targets.Length;index++)
            {
                vectorPrediction[index] = (int)Math.Round(predictions[index], 0) == targets[index] ? 1 : 0;
            }
            return vectorPrediction.Average();
        }

        private double Cost(double[] predictions, int[] targets)
        {
            if (predictions == null) throw new ArgumentNullException(nameof(predictions));
            if (targets == null) throw new ArgumentNullException(nameof(targets));

            var difference = new double[predictions.Length];
            for (var index  = 0;index  < predictions.Length; index++)
            {
                difference[index] = Math.Pow(predictions[index] - targets[index],2);
            }
            return difference.Average();
        }

        private double[] InitializeWeightsGradient(double predictions, int targets,double preActiovations, double[,] features)
        {
            var newWeights = new double[features.GetLength(1)];
            for (var rowIndex = 0; rowIndex < features.GetLength(1); rowIndex++)
            {
                
                newWeights[rowIndex] = (predictions - targets) * DerivativeActivation(new double[] { preActiovations }) * features[0, rowIndex];
            }
            return newWeights;
        }

        private double InitializeBiaisGradient(double predictions, int targets, double preActiovations)
        {
            return (predictions - targets) * DerivativeActivation(new double[] { preActiovations }); ;
        }

        private double DerivativeActivation(double[,] features, double[] weights, double biais)
        {
            
            var preActivation = PreActivation(features, weights, biais);
            var s = new double[preActivation.Length];
            for (var index = 0; index < preActivation.Length; index++)
                s[index] = 1 - preActivation[index];
            return preActivation.Multiply(s)[0];
        }

        private double DerivativeActivation(double[] z)
        {
            return Activation(z)[0] * (1 - Activation(z)[0]);
        }
        
        public void Train()
        {
            //var features = CreateData();
            //var targets = CreateTarget(100, 0).Concatenate(CreateTarget(100, 1));
            var features = LoadDataService.ReadCsvToMatrix(@"D:\Velompanahy\prog\ProgPython\Linear\features.csv");
            var targets = LoadDataService.ReadCsvToMatrix(@"D:\Velompanahy\prog\ProgPython\Linear\targets.csv").GetColumn(0).Select(d=>(int)d).ToArray();
            var epochs = 100;
            var learningRate = 0.1;
            var weights = new double[features.GetLength(1)];
            var bias = 0d;
            var rd = Accord.Math.Random.Generator.Random;
            for (var i = 0; i < weights.Length; i++)
                weights[i] = rd.NextDouble();
            //weights[0] = 0.05;
            //weights[1] = 0.2;

            var predict = Predict(features, weights, bias);
            Console.WriteLine($"Accuracy {Accuracy(predict, targets)} weights {string.Join(";",weights)} bias {bias}");
            for (int index = 0; index < epochs; index++)
            {
                if(index%10==0)
                {
                    var prediction = Predict(features, weights, bias);
                    var cost= Cost(prediction, targets);
                    Console.WriteLine($"Cost {cost}");
                }
                var weightsGradients = new double[weights.Length];
                var biasGradient = 0d;
                for (int rowIndex = 0; rowIndex < features.GetLength(0); rowIndex++)
                {
                    var matrixFeature = features.GetRow(rowIndex).ToMatrix();
                    var z = PreActivation(matrixFeature, weights, bias);
                    var y = Activation(z);
                    var x = new double[weightsGradients.Length];
                    var k= InitializeWeightsGradient(y[0], targets[rowIndex], z[0], matrixFeature); ;
                    for (var i = 0; i < weightsGradients.Length; i++)
                        x[i] = weightsGradients[i] + k[i];
                    weightsGradients = x;
                    biasGradient +=  InitializeBiaisGradient( y[0], targets[rowIndex], z[0]);
                }
                weights = weights.Subtract(weightsGradients.Multiply(learningRate));
                bias = bias - (learningRate * biasGradient);

                
            }
            var predict1 = Predict(features, weights, bias);
            Console.WriteLine($"Accuracy {Accuracy(predict, targets)} weights {string.Join(";", weights)} biais {bias}");
            Console.ReadLine();
        }

        private void DispalyMatrix(double[,] matrix)
        {
            for (var index = 0; index < matrix.GetLength(0); index++)
                Console.WriteLine($"[{string.Join(";", matrix.GetRow(index))}]");
        }

        private void DisplayVector(int[]vector)
        {
            Console.WriteLine("**********************");
            Console.WriteLine(string.Join(Environment.NewLine, vector));
        }

        public void Test()
        {
            var input = CreateData();
            var targets = CreateTarget(100, 0).Concatenate(CreateTarget(100,1));
            var session = new TFSession();
            var graph = session.Graph;
            var tfFeatures = graph.Placeholder(TFDataType.Double);
            var tfTargets = graph.Placeholder(TFDataType.Double);
            var b = graph.Placeholder(TFDataType.Int16, new TFShape(new long[3]));
            var bb = graph.Placeholder(TFDataType.Int16, new TFShape(new long[1]));


            var w1 = graph.Variable(graph.RandomNormal(new TFShape(new long[] { 2, 3 })));
            var b1 = graph.Variable(graph.ZerosLike(b));

            var z1 = graph.MatMul(tfFeatures, w1);
            var z1Add = graph.Add(z1, b1);
            var a1 = graph.Sigmoid(z1Add);

            var w2= graph.Variable(graph.RandomNormal(new TFShape(new long[] { 3, 1 })));
            var b2 = graph.Variable(graph.ZerosLike(bb));
            var z2 = graph.MatMul(a1, w2);
            var z2Add = graph.Add(z2, b2);
            var py = graph.Sigmoid(z2Add);

            var cost = graph.ReduceMean(graph.Square(graph.Sub(py, tfTargets)));
            var correctPred = graph.Equal(graph.Round(py), tfTargets);
            var accuracy = graph.ReduceMean(graph.Cast(correctPred, TFDataType.Float));

            


            var runner = session.GetRunner();
            runner.AddInput(tfFeatures, input);
            var mul =graph.Mul(tfFeatures, tfFeatures);
            var output=runner.Run(mul).GetValue();
           var w= graph.Variable(new TFShape(2, 1), TFDataType.Double);
          
        }

    }

   
}
