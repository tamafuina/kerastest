﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KerasTest;
using KerasTest.Keras;
using KerasTest.LoadData;
using Statistics;

namespace KerasTest
{
    class Program
    {
        static void Main(string[] args)
        {
            //var test = new TensoFlowSharp();
            //for(var index=0;index<5;index++)
            //test.Multiply();
            //var loadImage = new LoadDataService();
            //var images= loadImage.LoadTrafficImage(@"D:\Velompanahy\prog\KerasTest\BelgiumTSC_Training\Training");
            //var count = images.Count;
            var test = new TensoFlowSharp();
            //test.ConfigDevice(images.Values.ToArray());


            //test.Train();
            test.Test();
        }
    }
}
