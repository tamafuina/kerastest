﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CNTKTest
{
    public enum LossErrorType
    {
        CrossEntropyAndClassification,
        CrossEntropyAndReduceSum,
        MseAndMse,
        MseAndMae,
        MseAndR2,
        MseAndMseBasic
    }
}
