﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CNTK;
using CNTKTest.Helpers;

namespace CNTKTest.NeuralNetworkModel
{
    public class FastFeedNeuralNetwork : INeuralNetworkModel
    {
        public Trainer CreateTrainingNeuralNetwork(Function feature,Variable label, NNActivation neuralNetworkActivation,int numHiddenLayers,int hidenLayerDim, int numOutputClasses, string neuralModelName, DeviceDescriptor device,(LossErrorType lossErrorType, string lossFunctionName,string errorFunctionName) lossError, double trainingParameterValue= 0.001125, uint minibatchSize=1)
        {
            //Build simple Feed Froward Neural Network model
            var ffnn_model = NeuralNetworkHelpers.CreateFFNeuralNetwork(feature, numHiddenLayers, hidenLayerDim, numOutputClasses, neuralNetworkActivation, neuralModelName, device);

            //Loss and error functions definition

            var (lossFunction, erroFunction) = LossErrorHelpers.CreateLossErrorFunction(lossError.lossErrorType, ffnn_model, label, device, lossError.lossFunctionName, lossError.errorFunctionName);

            // set learning rate for the network
            var learningRatePerSample = new TrainingParameterScheduleDouble(trainingParameterValue, minibatchSize);

            //define learners for the NN model
            var ll = Learner.SGDLearner(ffnn_model.Parameters(), learningRatePerSample);

            //define trainer based on ffnn_model, loss and error functions , and SGD learner
            return Trainer.CreateTrainer(ffnn_model, lossFunction, erroFunction, new Learner[] { ll });
        }

        public void CreateTestingNeuralNetwork()
        {
            throw new NotImplementedException();
        }
    }
}
