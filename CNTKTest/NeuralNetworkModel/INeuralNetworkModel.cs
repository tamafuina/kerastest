﻿using CNTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CNTKTest.NeuralNetworkModel
{
    public interface INeuralNetworkModel
    {
        Trainer CreateTrainingNeuralNetwork(Function feature, Variable label, NNActivation neuralNetworkActivation, int numHiddenLayers, int hidenLayerDim, int numOutputClasses, string neuralModelName, DeviceDescriptor device, (LossErrorType lossErrorType, string lossFunctionName, string errorFunctionName) lossErrorType, double trainingParameterValue = 0.001125, uint minibatchSize = 1);
        void CreateTestingNeuralNetwork();
    }
}
