﻿using Encog.Engine.Network.Activation;
using Encog.ML.Data.Basic;
using Encog.Neural.Networks;
using Encog.Neural.Networks.Layers;
using Encog.Neural.Networks.Training.Propagation.Resilient;
using Statistics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CNTKTest.Encog
{
    public class EncogNeuralNetwork
    {

        public void Train(float[][] xTrain, float[][] yTrain,float[][]xTest, float[][]yTest)
        {
            Train(ConvertFloatToDouble(xTrain), ConvertFloatToDouble(yTrain), ConvertFloatToDouble(xTest), ConvertFloatToDouble(yTest));
        }
        public void Train(float[,]xTrain, float[,]yTrain,float[,]xTest,float[,]yTest)
        {
            Train(ConvertArrayToJagged(ConvertFloatToDouble(xTrain)), ConvertArrayToJagged(ConvertFloatToDouble(yTrain)), ConvertArrayToJagged(ConvertFloatToDouble(xTest)), ConvertArrayToJagged(ConvertFloatToDouble(yTest)));
        }
        public void Train(double[,]xTrain, double[,]yTrain,double[,]xTest, double[,]yTest)
        {
            Train(ConvertArrayToJagged(xTrain), ConvertArrayToJagged(yTrain), ConvertArrayToJagged(xTest), ConvertArrayToJagged(yTest));
        }
        public void Train(double[][]xTrain, double[][]yTrain, double[][]xTest, double[][]yTest)
        {
            if (xTrain == null)
                throw new ArgumentNullException(nameof(xTrain));
            if (yTrain == null)
                throw new ArgumentNullException(nameof(yTrain));
            if (xTest == null)
                throw new ArgumentNullException(nameof(xTest));
            if (yTest == null)
                throw new ArgumentNullException(nameof(yTest));

            var meansXTrained = Statistics.Statistics.Average(xTrain);
            var stdXTrained = Statistics.Statistics.StandardDeviation(xTrain);
            var meansYTrained = Statistics.Statistics.Average(yTrain);
            var stdYTrained = Statistics.Statistics.StandardDeviation(yTrain);
            var meansXTested = Statistics.Statistics.Average(xTest);
            var stdXTested = Statistics.Statistics.StandardDeviation(xTest);
            var meansYTested = Statistics.Statistics.Average(yTest);
            var stdYTested = Statistics.Statistics.StandardDeviation(yTest);
            var normalizedXTrain = Normalize.Normalized(xTrain, meansXTrained, stdXTrained);
            var normalizedYTrain = Normalize.Normalized(yTrain, meansYTrained, stdYTrained);
            var normalizedXTest = Normalize.Normalized(xTest, meansXTested, stdXTested);
            var normalizedYTest = Normalize.Normalized(yTest, meansYTested, stdYTested);
            var network = new BasicNetwork();
            network.AddLayer(new BasicLayer(null, true, xTrain[0].Length));
            network.AddLayer(new BasicLayer(new ActivationTANH(), true, 64));
            network.AddLayer(new BasicLayer(new ActivationTANH(), true, 64));
            network.AddLayer(new BasicLayer(new ActivationTANH(), true, 1));
            network.Structure.FinalizeStructure();
            network.Reset();

            //Create training Dataset   
            //var inputDataset = new double[input.Length][];
            //var labelDataset = new double[label.Length][];
            //for(var i=0;i<input.Length;i++)
            //{
            //    inputDataset[i] = new double[] { input[i] };
            //    labelDataset[i] = new double[] { label[i] };
            //}
            var trainingSet = new BasicMLDataSet(normalizedXTrain, normalizedYTrain);
            var testingSet = new BasicMLDataSet(normalizedXTest, normalizedYTest);

            var train = new ResilientPropagation(network, trainingSet);
            var epoch = 0;
            do
            {
                train.Iteration(); 
                if(epoch%20==0)
                    Console.WriteLine(@"Epoch #" + epoch + @" Error:" + train.Error);
                epoch++;
            } while (train.Error >= 0.01 && epoch<200);

            Console.WriteLine(@"Neural Network Results:");
            foreach (var pair in testingSet)
            {
                var output = network.Compute(pair.Input);
                Console.WriteLine(@", actual=" +Normalize.UnNormalized( output[0],meansYTested[0],stdYTested[0]) + @",ideal=" +      Normalize.UnNormalized( pair.Ideal[0], meansYTested[0], stdYTested[0]));
            }
        }

        private T[][] ConvertArrayToJagged<T>(T[,]array)
        {
            var newArray = new T[array.GetLength(0)][];
            for(var i=0;i<array.GetLength(0);i++)
            {
                var item = new T[array.GetLength(1)];
                for (var j = 0; j < array.GetLength(1); j++)
                    item[j] = array[i, j];
                newArray[i] = item;
            }
            return newArray;
        }

        private double[,] ConvertFloatToDouble(float[,]array)
        {
            var newArray = new double[array.GetLength(0), array.GetLength(1)];
            for (var i = 0; i < array.GetLength(0); i++)
                for (var j = 0; j < array.GetLength(1); j++)
                    newArray[i, j] = array[i, j];
            return newArray;
        }

        private double[][] ConvertFloatToDouble(float[][] array)
        {
            var newArray = new double[array.GetLength(0)][];
            for (var i = 0; i < array.GetLength(0); i++)
            {
                var item = new double[array[i].Length];
                for (var j = 0; j < array[i].Length; j++)
                    item[j] = array[i][ j];
                newArray[i] = item;
            }
            return newArray;
        }
    }
}
