﻿using CNTKTest.Classification;
using CNTKTest.Encog;
using CNTKTest.Helpers;
using CNTKTest.Regression;
using Statistics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CNTKTest
{
    class Program
    {
        static void Main(string[] args)
        {

            //var featuresData = new double[170, 5];
            //var labelsData = new double[170, 2];
            //var random = new Random(1);
            //for (var rowIndex = 0; rowIndex < featuresData.GetLength(0); rowIndex++)
            //{
            //    for (var colIndex = 0; colIndex < featuresData.GetLength(1); colIndex++)
            //        featuresData[rowIndex, colIndex] = random.NextDouble();
            //    for (var colIndex = 0; colIndex < labelsData.GetLength(1); colIndex++)
            //        labelsData[rowIndex, colIndex] = (rowIndex % 2 == 0 ? 2d : 1d) * random.NextDouble();
            //}
            //TextWriter.Write("test.txt", featuresData, labelsData, "features", "label");
            //var logReg = new LogisticRegression();//new IrisClassification();
            //logReg.Train();

            var linearRegression = new LinearRegression();
            linearRegression.LoadHousePriceData(out var xTrain, out var yTrain, out var xTest, out var yTest);
            //var house = new HouseRegression();
            //house.HouseData(out var xTrain, out var yTrain, out var xTest, out var yTest);
            linearRegression.TrainMultipleInput(xTrain, yTrain, xTest, yTest);

            Console.WriteLine("***************");
            var encog = new EncogNeuralNetwork();
            encog.Train(xTrain, yTrain, xTest, yTest);

            //var mnist = new MnistClassification();
            //mnist.TrainFromTensorflow();
            Console.WriteLine("***************");
            //mnist.Train();

            Console.ReadLine();
        }
    }
}
