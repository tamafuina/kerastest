﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CNTKTest
{
    public enum NNActivation
    {
        None,
        Relu,
        Sigmoid,
        Tanh
    }
}
