﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CNTKTest.Helpers
{
    public class TextWriter
    {
        public static void Write(string addressFile,double[,]featuresData,double[,]labelsData,string featuresName,string labelsName)
        {
            if (featuresData == null) throw new ArgumentNullException(nameof(featuresData));
            if (labelsData == null) throw new ArgumentNullException(nameof(labelsData));
            if (featuresName == null) throw new ArgumentNullException(nameof(featuresName));
            if (labelsName == null)  throw new ArgumentNullException(nameof(labelsName));

            if (featuresData.GetLength(0) != labelsData.GetLength(0)) throw new ArgumentException("size data problem");
            using (var streamWriter = new StreamWriter(addressFile))
            {
                for (var rowIndex = 0; rowIndex < featuresData.GetLength(0); rowIndex++)
                {
                    streamWriter.Write($"|{featuresName} ");
                    for (var colIndex = 0; colIndex < featuresData.GetLength(1); colIndex++)
                    {
                        streamWriter.Write($"{featuresData[rowIndex, colIndex].ToString(CultureInfo.InvariantCulture)} ");
                    }
                    streamWriter.Write($"|{labelsName} ");
                    for (var colIndex = 0; colIndex < labelsData.GetLength(1); colIndex++)
                    {
                        streamWriter.Write($"{labelsData[rowIndex, colIndex].ToString(CultureInfo.InvariantCulture)} ");
                    }
                        streamWriter.WriteLine();
                }
            }
        }
    }
}
