﻿using CNTK;
using System;

namespace CNTKTest.Helpers
{
    public class Util
    {
        public static Function Dense(Variable input, int outputDim, DeviceDescriptor device, string outputName = "")
        {
            var shape = NDShape.CreateNDShape(new int[] { outputDim, NDShape.InferredDimension });
            var timesParam = new Parameter(shape, DataType.Float, CNTKLib.GlorotUniformInitializer(CNTKLib.DefaultParamInitScale, CNTKLib.SentinelValueForInferParamInitRank, CNTKLib.SentinelValueForInferParamInitRank, 1), device, "timesParam_" + outputName);
            var timesFunction = CNTKLib.Times(timesParam, input, 1 /* output dimension */, 0 /* CNTK should infer the input dimensions */);
            var plusParam = new Parameter(NDShape.CreateNDShape(new int[] { NDShape.InferredDimension }), 0.0f, device, "plusParam_" + outputName);
            var result = CNTKLib.Plus(plusParam, timesFunction, outputName);

            return result;
        }


        public static Function Dense(Variable input, int outputDim, DeviceDescriptor device, NNActivation activation, string layerName = "")
        {
            var weights = new Parameter(NDShape.CreateNDShape(new int[] { outputDim, input.Shape[0] }), DataType.Float, CNTKLib.GlorotUniformInitializer(
        CNTKLib.DefaultParamInitScale,
        CNTKLib.SentinelValueForInferParamInitRank,
        CNTKLib.SentinelValueForInferParamInitRank, 1));
            var biais = new Parameter(NDShape.CreateNDShape(new int[] { outputDim }), DataType.Float, 0);
            var y = CNTKLib.Plus(CNTKLib.Times(weights, input), biais, layerName);
            switch (activation)
            {
                case NNActivation.Relu:
                    return CNTKLib.ReLU(y);
                case NNActivation.Sigmoid:
                    return CNTKLib.Sigmoid(y);
                case NNActivation.Tanh:
                    return CNTKLib.Tanh(y);
                case NNActivation.None:
                    return y;
                default:
                    return y;
            }
        }

        public static CNTK.NDArrayView[] GetMinibatchDataCPU(NDShape shape, float[][] src, int[] indices, int indices_begin, int indices_end)
        {

            var num_indices = indices_end - indices_begin;

            var row_length = shape.TotalSize;

            var result = new NDArrayView[num_indices];



            var row_index = 0;

            for (var index = indices_begin; index != indices_end; index++)
            {

                var dataBuffer = src[indices[index]];

                var ndArrayView = new CNTK.NDArrayView(shape, dataBuffer, CNTK.DeviceDescriptor.CPUDevice, true);

                result[row_index++] = ndArrayView;

            }

            return result;

        }

        public static float[] GetMinibatchDataCPU(CNTK.NDShape shape, float[] src, int[] indices, int indices_begin, int indices_end)
        {

            var num_indices = indices_end - indices_begin;

            var row_length = shape.TotalSize;

            var result = new float[num_indices];

            var row_index = 0;

            for (var index = indices_begin; index != indices_end; index++)
            {

                result[row_index++] = src[indices[index]];

            }

            return result;

        }

        public static NDArrayView[] GetMinibatchDataCPU(NDShape shape, float[][] src, int indicesBegin, int indicesEnd)
        {
            var numIndices = indicesEnd - indicesBegin;
            var result = new NDArrayView[numIndices];

            var rowIndex = 0;
            for (var index = indicesBegin; index != indicesEnd; index++)
            {
                var dataBuffer = src[index];
                var ndArrayView = new NDArrayView(shape, dataBuffer, DeviceDescriptor.CPUDevice, true);
                result[rowIndex++] = ndArrayView;
            }

            return result;

        }

        public static void Swap<T>(T[] array, int n, int k)
        {
            var temp = array[n];
            array[n] = array[k];
            array[k] = temp;
        }



        public static void Shuffle<T>(T[] array)
        {
            // https://stackoverflow.com/a/110570

            var rng = new Random();
            var n = array.Length;
            while (n > 1)
            {
                var k = rng.Next(n--);
                Swap(array, n, k);
            }

        }



        public static void Shuffle<T1, T2>(T1[] array1, T2[] array2)
        {
            if (array1 == null) throw new ArgumentNullException(nameof(array1));
            if (array2 == null) throw new ArgumentNullException(nameof(array2));
            System.Diagnostics.Debug.Assert(array1.Length == array2.Length);

            var rng = new Random();
            var n = array1.Length;
            while (n > 1)
            {
                var k = rng.Next(n--);
                Swap(array1, n, k);
                Swap(array2, n, k);
            }

        }

        public static int[] ShuffledIndices(int N)
        {

            var array = new int[N];
            for (int i = 0; i < N; i++)
            {
                array[i] = i;
            }

            Shuffle(array);

            return array;

        }

        public static Value GetTensors(NDShape shape, float[][] src, int indicesBegin, int indicesEnd, DeviceDescriptor device)
        {
            var cpuTensors = GetMinibatchDataCPU(shape, src, indicesBegin, indicesEnd);
            var result = Value.Create(shape, cpuTensors, device, true);
            return result;

        }

        public static CNTK.Value GetTensors(CNTK.NDShape shape, float[][] src, int[] indices, int indices_begin, int indices_end, CNTK.DeviceDescriptor device)
        {

            var cpu_tensors = Util.GetMinibatchDataCPU(shape, src, indices, indices_begin, indices_end);

            var result = CNTK.Value.Create(shape, cpu_tensors, device, true);

            return result;


        }

        public static CNTK.Value get_tensors(CNTK.NDShape shape, float[][] src, int[] indices, int indices_begin, int indices_end, CNTK.DeviceDescriptor device)
        {
            var cpu_tensors = Util.get_minibatch_data_CPU(shape, src, indices, indices_begin, indices_end);
            var result = CNTK.Value.Create(shape, cpu_tensors, device, true);
            return result;
        }

        static CNTK.NDArrayView[] get_minibatch_data_CPU(CNTK.NDShape shape, float[][] src, int[] indices, int indices_begin, int indices_end)
        {
            var num_indices = indices_end - indices_begin;
            var row_length = shape.TotalSize;
            var result = new CNTK.NDArrayView[num_indices];

            var row_index = 0;
            for (var index = indices_begin; index != indices_end; index++)
            {
                var dataBuffer = src[indices[index]];
                var ndArrayView = new CNTK.NDArrayView(shape, dataBuffer, CNTK.DeviceDescriptor.CPUDevice, true);
                result[row_index++] = ndArrayView;
            }
            return result;
        }
    }
}
