﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using Accord.IO;

namespace CNTKTest.Helpers
{
    public  class LoadDataService
    {
        public static double[,] ReadCsvToMatrix(string fileName, bool hasHeader=false)
        {
            var csv = File.ReadAllText(fileName);
            var reader = CsvReader.FromText(csv, hasHeader);


            var count = 0;
            var inputList = reader.ToList();
            var data = new double[inputList.Count, reader.FieldCount];

            foreach (var line in inputList)
            {
                for (var index = 0; index < reader.FieldCount; index++)
                    data[count, index] = Convert.ToDouble(line[index], CultureInfo.GetCultureInfo("en-En").NumberFormat);
                count++;
            }

            return data;
        }
    }
}
