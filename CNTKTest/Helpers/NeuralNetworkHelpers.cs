﻿using CNTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CNTKTest.Helpers
{
    public class NeuralNetworkHelpers
    {
        public static Function ApplyActivationFunction(Function layer, NNActivation actuFunction)
        {
            if (layer == null) throw new ArgumentNullException(nameof(layer));

            switch (actuFunction)
            {
                case NNActivation.Relu:
                    return CNTKLib.ReLU(layer);
                case NNActivation.Sigmoid:
                    return CNTKLib.Sigmoid(layer);
                case NNActivation.Tanh:
                    return CNTKLib.Tanh(layer);
                case NNActivation.None:
                default:
                    return layer;
            }
        }
        public static Function SimpleLayer(Function input, int outputDim, DeviceDescriptor device)
        {
            if (input == null) throw new ArgumentNullException(nameof(input));
            if (device == null) throw new ArgumentNullException(nameof(device));

            var gloroInit = CNTKLib.GlorotUniformInitializer(
                CNTKLib.DefaultParamInitScale,
                CNTKLib.SentinelValueForInferParamInitRank,
                CNTKLib.SentinelValueForInferParamInitRank, 1);

            //Create weights and bias
            var var = (Variable)input;            
            var weights = new Parameter(new int[] { outputDim, var.Shape[0] }, DataType.Float, gloroInit, device,"w");
            var bias = new Parameter(new NDShape(1, outputDim),DataType.Float, 0, device, "b");

            return CNTKLib.Times(weights, input) + bias;
        }

        public static Function CreateFFNeuralNetwork(Function input, int hiddenLayerCount, int hiddenDim, int outputDim, NNActivation actuFunction, string modelName, DeviceDescriptor device)
        {
            if (input == null) throw new ArgumentNullException(nameof(input));
            if (device == null) throw new ArgumentNullException(nameof(device));

            var gloroInit = CNTKLib.GlorotUniformInitializer(
               CNTKLib.DefaultParamInitScale,
               CNTKLib.SentinelValueForInferParamInitRank,
               CNTKLib.SentinelValueForInferParamInitRank, 1);

            var h = SimpleLayer(input, hiddenDim, device);
            h = ApplyActivationFunction(h, actuFunction);

            for (var i = 1; i < hiddenLayerCount; i++)
            {
                h = SimpleLayer(h, hiddenDim, device);
                h = ApplyActivationFunction(h, actuFunction);
            }

            //Output layer
            var r = SimpleLayer(h, outputDim, device);
            r.SetName(modelName);

            return r;
        }

    }
}
