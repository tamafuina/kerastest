﻿using CNTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CNTKTest.Helpers
{
    public class UtilTensor
    {

        public static Value GetTensor(int start, int end, float[][] data, IList<int> indices)
        {
            var array = CreateBatchDataArrayView(start, end, data, indices, DeviceDescriptor.CPUDevice);
            return CNTK.Value.Create(new[] { data[0].Length }, array, DeviceDescriptor.CPUDevice, true);
        }

        public static IEnumerable<float> CreateBatchData(int start, int end, IEnumerable<float> data, IList<int> indices)
        {
            if (end > data.Count()) end = data.Count();
            var size = end - start;
            var newData = new float[size];
            var arrayData = data.ToList();
            var index = 0;
            for (var i = start; i < end; i++)
            {
                newData[index] = arrayData[indices[i]];
                index++;
            }
            return newData;
        }

        public static IEnumerable<float> CreatBatchDataFromJaggedArray(int start, int end, float[][] data, IList<int> indices)
        {
            if (data == null) throw new ArgumentNullException();
            if (data.Length == 0) throw new ArgumentException();
            if (end > data.Count()) end = data.Count();
            var size = end - start;
            var newData = new float[size * data[0].Length];
            var arrayData = data.ToList();
            var index = 0;
            for (var i = start; i < end; i++)
            {
                for (var j = 0; j < data[i].Length; j++)
                {
                    newData[index] = data[indices[i]][j];
                    index++;
                }
            }
            return newData;
        }

        public static IEnumerable<IEnumerable<float>> CreateBatchSequenceArray(int start, int end, float[][]data, IList<int>indices)
        {
            if (data == null) throw new ArgumentNullException();
            if (data.Length == 0) throw new ArgumentException();
            if (end > data.Count()) end = data.Count();
            var size = end - start;
            var newData = new List<IList<float>>();
            for(var i=start;i<end;i++)
            {
                var item = new List<float>();
                for(var j=0;j<data[i].Length;j++)
                {
                    item.Add(data[i][j]);
                }
                newData.Add(item);
            }
            return newData;
        }

        private static NDArrayView[] CreateBatchDataArrayView(int start, int end, float[][] data, IList<int> indices, DeviceDescriptor device)
        {
            if (data == null) throw new ArgumentNullException();
            if (data.Length == 0) throw new ArgumentException();
            if (end > data.Count()) end = data.Count();
            var size = end - start;
            var arrayView = new NDArrayView[data.Length];
            var index = 0;
            for (var i = start; i < end; i++)
            {
                var item = new float[data[i].Length];
                for (var j = 0; j < data[i].Length; j++)
                {
                    item[j] = data[indices[i]][j];
                    index++;
                }
                arrayView[i] = new NDArrayView(NDShape.CreateNDShape(new[] { data[i].Length }), item, device, true);
            }
            return arrayView;
        }
    }
}
