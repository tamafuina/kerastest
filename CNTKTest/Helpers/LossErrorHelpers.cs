﻿using CNTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CNTKTest.Helpers
{
    public class LossErrorHelpers
    {
        public static (Function lossFunction,Function erroFunction) CreateLossErrorFunction(LossErrorType lossErrorType, Function model,Variable label, DeviceDescriptor device, string lossFunctionName="lossFunction",string errorFunctionName="errorFunction")
        {
            switch (lossErrorType)
            {
                case LossErrorType.CrossEntropyAndClassification:
                    return (CNTKLib.CrossEntropyWithSoftmax(new Variable(model), label, lossFunctionName), CNTKLib.ClassificationError(new Variable(model), label, errorFunctionName));
                case LossErrorType.CrossEntropyAndReduceSum:
                    return (CNTKLib.CrossEntropyWithSoftmax(new Variable(model), label, lossFunctionName), CNTKLib.ReduceSum(CNTKLib.Minus(new Variable(model), label), Axis.AllAxes(),errorFunctionName));
                case LossErrorType.MseAndMse:
                    return (MeanSquareError(model.Output, label), MeanSquareError(model.Output, label));
                case LossErrorType.MseAndMae:
                    return (MeanSquareError(model.Output, label), MeanAbsoluteError(model.Output, label));
                case LossErrorType.MseAndR2:
                    return (MeanSquareError(model.Output, label), R2(model.Output, label, device));
                case LossErrorType.MseAndMseBasic:
                    return (MeanSquareError(model.Output, label), MeanSquareErrorBasic(model.Output, label));
                default:
                    return (null, null);
            }
        }

        private static Function MeanSquareError(Variable predicted, Variable actual)
        {
            return CNTKLib.ReduceMean(CNTKLib.Square(CNTKLib.Minus(predicted, actual)), new Axis(0));
        }

        private static Function MeanAbsoluteError(Variable predicted, Variable actual)
        {
            return CNTKLib.ReduceMean(CNTKLib.Abs(CNTKLib.Minus(predicted, actual)), new Axis(0));
        }

        private static Function R2(Variable predicted, Variable actual, DeviceDescriptor device)
        {
            var sse = CNTKLib.ReduceSum( CNTKLib.Square(CNTKLib.Minus(predicted, CNTKLib.ReduceMean(predicted,new Axis(0)))),new Axis(0));
            var sso= CNTKLib.ReduceSum( CNTKLib.Square(CNTKLib.Minus(actual, CNTKLib.ReduceMean(actual,new Axis(0)))),new Axis(0));
            var constant = Constant.Scalar(1f, device);
            return CNTKLib.ElementDivide(sse, sso);//CNTKLib.Minus(constant, CNTKLib.ElementDivide(sse, sso));
        }
        private static Function MeanSquareErrorBasic(Variable predicted, Variable actual)
        {
            return CNTKLib.ReduceMean(CNTKLib.Square(CNTKLib.Minus(predicted, CNTKLib.ReduceMean(actual, new Axis(0)))), new Axis(0));
        }
    }
}
