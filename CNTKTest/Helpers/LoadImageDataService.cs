﻿using System;
using System.IO;

namespace CNTKTest.Helpers
{
    public class LoadImageDataService
    {
        public static float[][] LoadBinaryFile(string filePath, int rowNumbers, int colNumbers)
        {
            var buffer = new byte[sizeof(float) * rowNumbers * colNumbers];
            using (var reader = new BinaryReader(File.OpenRead(filePath)))
            {
                reader.Read(buffer, 0, buffer.Length);
            }
            var dst = new float[rowNumbers][];
            for (var index = 0; index < rowNumbers; index++)
            {
                dst[index] = new float[colNumbers];
                Buffer.BlockCopy(buffer, index * colNumbers *sizeof(float), dst[index], 0, colNumbers *sizeof(float));
            }
            return dst;
        }

        public static float[][] load_binary_file(string filepath, int numRows, int numColumns)
        {
            Console.WriteLine("Loading " + filepath);
            var buffer = new byte[sizeof(float) * numRows * numColumns];
            using (var reader = new System.IO.BinaryReader(System.IO.File.OpenRead(filepath)))
            {
                reader.Read(buffer, 0, buffer.Length);
            }
            var dst = new float[numRows][];
            for (int row = 0; row < dst.Length; row++)
            {
                dst[row] = new float[numColumns];
                System.Buffer.BlockCopy(buffer, row * numColumns * sizeof(float), dst[row], 0, numColumns * sizeof(float));
            }
            return dst;
        }
    }
}
