﻿using CNTK;
using CNTKTest.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using Statistics;
using System.Text;
using System.Threading.Tasks;

namespace CNTKTest.Regression
{
    public class LinearRegression
    {
        public IEnumerable<double> Features { get; private set; }
        public IEnumerable<double> Labels { get; private set; }
        public void Train()
        {
            var device = DeviceDescriptor.UseDefaultDevice();
            var epoch = 200;

            var x = Variable.InputVariable(new[] { 1 }, DataType.Float, "input");
            var y = Variable.InputVariable(new[] { 1 }, DataType.Float, "output");
            //var xList = CreateRandomizedData(1, 5, 15);
            //var yList = CreateRandomizedData(3, 11, 15);
            //var xValues = Value.CreateBatch(new NDShape(1, 1), xList, device);
            //Console.WriteLine($"xList: {string.Join(",", xList)}");
            //Console.WriteLine($"yList: {string.Join(",", yList)}");
            //var yValues = Value.CreateBatch(new NDShape(1, 1), yList, device);

            var linearRegression = CreateLinearRegressionModel(x, device);
            var paramValues = linearRegression.Inputs.Where(p => p.IsParameter);
            var totalParameters = paramValues.Sum(c => c.Shape.TotalSize);
            Console.WriteLine($"Linear Regression Model has {totalParameters} parameters, {paramValues.ElementAt(0).Name} and {paramValues.ElementAt(1).Name}");

            var lossFunction = CNTKLib.SquaredError(linearRegression, y);
            var evalFunction = CNTKLib.SquaredError(linearRegression, y);
            var trainer = CreateTrainer(linearRegression, lossFunction, evalFunction);
            var batchSize = 64;
            var xList = CreateData(0, 50, -4, 4, 1500);
            var yList = CreateData(0, 50, -8, 8, 1500);
            Features = xList.Select(f => (double)f);
            Labels = yList.Select(f => (double)f);
            Console.Write($"XList {Environment.NewLine} {string.Join(",", xList.Take(100))} {Environment.NewLine}");
            Console.Write($"YList {Environment.NewLine} {string.Join(",", yList.Take(100))} {Environment.NewLine}");

            for (var i = 0; i <= epoch; i++)
            {
                var pos = 0;
                var indices = Shuffle(1500);
                while (pos < xList.Count())
                {
                    var posEnd = Math.Min(pos + batchSize, xList.Count());
                    var xValues = Value.CreateBatch(new NDShape(1, 1), UtilTensor.CreateBatchData(pos, posEnd, xList, indices), device);
                    var yValues = Value.CreateBatch(new NDShape(1, 1), UtilTensor.CreateBatchData(pos, posEnd, yList, indices), device);
                    var dictionaryValue = new Dictionary<Variable, Value>()
                {
                    {x,xValues },
                    {y,yValues }
                };
                    trainer.TrainMinibatch(dictionaryValue, true, device);
                    pos = posEnd;
                }
                var loss = trainer.PreviousMinibatchLossAverage();
                var eval = trainer.PreviousMinibatchEvaluationAverage();
                if (i % 20 == 0)
                    Console.WriteLine($"It= {i}, Loss= {loss}, Eval= {eval}");
                if (i == epoch)
                {
                    var bValue = new Value(paramValues.ElementAt(0).GetValue()).GetDenseData<float>(paramValues.ElementAt(0))[0][0];
                    var wValue = new Value(paramValues.ElementAt(1).GetValue()).GetDenseData<float>(paramValues.ElementAt(1))[0][0];
                    Console.WriteLine();
                    Console.WriteLine($"Training process finished with the following regression parameters:");
                    Console.WriteLine($"b={bValue} w={wValue}");
                }
            }
        }

        private static Function CreateLinearRegressionModel(Variable x, DeviceDescriptor device)
        {
            var init = CNTKLib.GlorotUniformInitializer(1.0, 1, 0, 1);
            var b = new Parameter(new NDShape(1, 1), DataType.Float, init, device, "b");
            var w = new Parameter(new NDShape(2, 1), DataType.Float, init, device, "w");
            var wX = CNTKLib.Times(w, x, "wX");
            var l = CNTKLib.Plus(wX, b, "wX+b");
            return l;

        }

        private Trainer CreateTrainer(Function network, Function loss, Function evaluation)
        {
            var parameterVector = new CNTK.ParameterVector((System.Collections.ICollection)network.Parameters());
            var learner = CNTK.CNTKLib.AdamLearner(parameterVector, new CNTK.TrainingParameterScheduleDouble(0.001, 1), new CNTK.TrainingParameterScheduleDouble(0.9, 1), true);
            var learningRateValue = 0.082;
            var learningRate = new TrainingParameterScheduleDouble(learningRateValue);
            var zParams = new ParameterVector(network.Parameters().ToList());



            var learnersParameters = new List<Learner>
            {
                // var learners = Learner.SGDLearner(network.Parameters(), learningRate);
                learner
            };

            var trainer = Trainer.CreateTrainer(network, loss, evaluation, learnersParameters);

            return trainer;
        }

        private void Shuffle<T>(IList<T> list)
        {
            var random = new Random();
            var count = list.Count;
            for (var i = 0; i < list.Count; i++)
            {
                var r = random.Next(i, count - 1);
                var temp = list[i];
                list[i] = list[r];
                list[r] = temp;
            }
        }

        private IList<int> Shuffle(int size)
        {
            var random = new Random();
            var list = new int[size];

            for (var i = 0; i < size; i++)
            {
                list[i] = random.Next(i, size - 1);
            }
            return list;
        }

        private IEnumerable<float> CreateRandomizedData(int minValue, int maxValue, int size)
        {
            var list = new float[size];
            var random = new Random();
            for (var i = 0; i < size; i++)
            {
                list[i] = (float)random.Next(minValue, maxValue);
            }
            return list;
        }

        private IEnumerable<float> CreateData(int minValue, int maxValue, int minNoise, int maxNoise, int size)
        {
            var list = new float[size];
            var random = new Random();
            for (var i = 0; i < size; i++)
                list[i] += (float)random.Next(minNoise, maxNoise);
            return list;
        }

        private IList<int> CreateIndice(int length)
        {
            var list = new List<int>();
            for (var i = 0; i < length; i++)
                list.Add(i);
            return list;
        }

        public void TrainMultipleInput(float[][]xTrain,float[][]yTrain,float[][]xTest,float[][]yTest)
        {
            var device = DeviceDescriptor.UseDefaultDevice();
            //Load Data
            //LoadHousePriceData(out var xTrain, out var yTrain, out var xTest, out var yTest);

            //Create Input and Output variables
            var inputFeautures = Variable.InputVariable(new[] { xTrain[0].Length }, DataType.Float, "features");
            var outputLabels = Variable.InputVariable(new[] { yTrain[0].Length }, DataType.Float, "labels");

            //Normalize Data
            var meansXTrained = Statistics.Statistics.Average(xTrain);
            var stdXTrained = Statistics.Statistics.StandardDeviation(xTrain);
            var meansYTrained = Statistics.Statistics.Average(yTrain);
            var stdYTrained = Statistics.Statistics.StandardDeviation(yTrain);
            var meansXTest = Statistics.Statistics.Average(xTest);
            var stdXTest = Statistics.Statistics.StandardDeviation(xTest);
            var meansYTest = Statistics.Statistics.Average(yTest);
            var stdYTest= Statistics.Statistics.StandardDeviation(yTest);            
            var normalizedXTrain = Normalize.Normalized(xTrain, meansXTrained, stdXTrained);
            var normalizedYTrain = Normalize.Normalized(yTrain, meansYTrained, stdYTrained);
            var normalizedXTest = Normalize.Normalized(xTest, meansXTest, stdXTest);
            var normalizedYTest = Normalize.Normalized(yTest, meansYTest, stdYTest);

            //Create Model
            var network = CreateModel(inputFeautures, outputLabels, NNActivation.Tanh, device, "HousePriceModel", new Dictionary<string, (int, NNActivation)> { { "hidden1", (13, NNActivation.Relu) }, { "hidden2", (13, NNActivation.Relu) } });
            //var network = CNTK.CNTKLib.ReLU(Util.Dense(inputFeautures, 64, device));
            //network = CNTK.CNTKLib.ReLU(Util.Dense(network, 64, device));
            //network = Util.Dense(network, 1, device);

            //Train Model
            var (lossFunction, erroFunction) = LossErrorHelpers.CreateLossErrorFunction(LossErrorType.MseAndMse, network, outputLabels, device, "lossFunction", "evalFunction");
            var trainer = CreateTrainer(network, lossFunction, erroFunction);
            var evaluator = CNTKLib.CreateEvaluator(erroFunction);
            var epoch = 200;
            var batchSize = 64;
            var epochAccuracy = 0d;
            var ii = 0;
            for (var i = 0; i <= epoch; i++)
            {


                var indices = Shuffle(normalizedXTrain.Length).ToArray();
                CreateTraining(device, normalizedXTrain, normalizedYTrain, inputFeautures, outputLabels, trainer, batchSize, indices);
                var evalAccuracy =CreateEvaluation(evaluator, inputFeautures, outputLabels, normalizedXTest, normalizedYTest, Shuffle(normalizedXTest.Length).ToArray(), batchSize, device);
                var loss = trainer.PreviousMinibatchLossAverage();
                var eval = trainer.PreviousMinibatchEvaluationAverage();
                epochAccuracy += eval;
                ii++;
                if (i % 20 == 0)
                    Console.WriteLine($"It= {i}, Loss= {loss}, Eval= {eval}, Eval accuracy={evalAccuracy}");

            }
            var indicesTest = Shuffle(normalizedXTest.Length).ToArray();

            Console.WriteLine($" accuracy {epochAccuracy / ii}");
            //var inputs = new Dictionary<CNTK.Variable, CNTK.Value>() { { x, x_minibatch } };
            //var outputs = new Dictionary<CNTK.Variable, CNTK.Value>() { { f.Output, null } };
            var inputDataMap = new Dictionary<Variable, Value>() {
             { inputFeautures, Value.CreateBatchOfSequences(inputFeautures.Shape, UtilTensor.CreateBatchSequenceArray(0, normalizedXTest.Length, normalizedXTest, CreateIndice(normalizedXTest.Length)),device) }};

            var outputDataMap = new Dictionary<Variable, Value>() {
            { network.Output, null }
        };
            network.Evaluate(inputDataMap, outputDataMap, device);
            var prediction = outputDataMap[network.Output].GetDenseData<float>(network.Output);
            var unNormalizedPrediction = new List<IList<float>>();
            for (var i = 0; i < yTest.Length; i++)
            {
                var list = new List<float>();
                for (var j = 0; j < yTest[i].Length; j++)
                {
                    var value = Normalize.UnNormalized(prediction[i][j], meansYTest[j], stdYTest[j]);
                    list.Add(value);
                    Console.WriteLine($"Predicted value: {value} , Actual value {yTest[i][j]}");
                }
                unNormalizedPrediction.Add(list);
            }
            var predicted = ArrayHelpers.ArrayToVector(ArrayHelpers.ListToArray(unNormalizedPrediction), 0).ToArray();
            var actual = ArrayHelpers.ArrayToVector(yTest, 0).ToArray();
            var r2=StatisticsEstimator.R2(predicted,actual);
            var mse = StatisticsEstimator.Mse(predicted, actual);
            Console.WriteLine($"MSE={mse}, R2={r2}");
        }



        private Function CreateModel(Variable input, Variable output, NNActivation outputActivation, DeviceDescriptor device, string modelName = "", IDictionary<string, (int numberNeuron, NNActivation activation)> hiddenLayers = null)
        {
            var network = Util.Dense(input, input.Shape.Dimensions[0], device, modelName);
            if (hiddenLayers != null)
                foreach (var layer in hiddenLayers)
                {
                    network = Util.Dense(network, layer.Value.numberNeuron, device, layer.Value.activation, layer.Key);
                }
            network = Util.Dense(network, output.Shape.Dimensions[0], device, outputActivation);
            return network;
        }

        Function Dense(CNTK.Variable input, int outputDim, CNTK.DeviceDescriptor device, string outputName = "")
        {
            var shape = CNTK.NDShape.CreateNDShape(new int[] { outputDim, CNTK.NDShape.InferredDimension });
            var timesParam = new CNTK.Parameter(shape, CNTK.DataType.Float, CNTK.CNTKLib.GlorotUniformInitializer(CNTK.CNTKLib.DefaultParamInitScale, CNTK.CNTKLib.SentinelValueForInferParamInitRank, CNTK.CNTKLib.SentinelValueForInferParamInitRank, 1), device, "timesParam_" + outputName);
            var timesFunction = CNTK.CNTKLib.Times(timesParam, input, 1 /* output dimension */, 0 /* CNTK should infer the input dimensions */);
            var plusParam = new CNTK.Parameter(CNTK.NDShape.CreateNDShape(new int[] { CNTK.NDShape.InferredDimension }), 0.0f, device, "plusParam_" + outputName);
            var result = CNTK.CNTKLib.Plus(plusParam, timesFunction, outputName);
            return result;
        }

        CNTK.Value Get_tensors(CNTK.NDShape shape, float[][] src, int[] indices, int indices_begin, int indices_end, CNTK.DeviceDescriptor device)
        {
            var cpu_tensors = Util.GetMinibatchDataCPU(shape, src, indices, indices_begin, indices_end);
            var result = CNTK.Value.Create(shape, cpu_tensors, device, true);
            return result;
        }

        private void CreateTraining(DeviceDescriptor device, float[][] xTrain, float[][] yTrain, Variable inputFeautures, Variable outputLabels, Trainer trainer, int batchSize, int[] indices)
        {
            var pos = 0;
            while (pos < xTrain.Length)
            {
                var posEnd = Math.Min(pos + batchSize, xTrain.Length);
                var xValues = Value.CreateBatchOfSequences(inputFeautures.Shape, UtilTensor.CreateBatchSequenceArray(pos, posEnd, xTrain, indices), device);//get_tensors(inputFeautures.Shape, xTrain, indices, pos, posEnd, device);
                var yValues = Value.CreateBatchOfSequences(outputLabels.Shape, UtilTensor.CreateBatchSequenceArray(pos, posEnd, yTrain, indices), device);//get_tensors(outputLabels.Shape, yTrain, indices, pos, posEnd, device);
                var dictionaryValue = new Dictionary<Variable, Value>()
                {
                    {inputFeautures,xValues },
                    {outputLabels,yValues }
                };
                trainer.TrainMinibatch(dictionaryValue, true, device);
                pos = posEnd;
            }
        }
        double CreateEvaluation(Evaluator evaluator, Variable inputFeatures, Variable outputLabels, float[][] xTest, float[][] yTest, int[] indices, int batchSize, DeviceDescriptor device)
        {
            var epochAccuracy = 0d;
            var pos = 0;

            while (pos < xTest.Length)
            {
                var posEnd = Math.Min(pos + batchSize, xTest.Length);
                var xValues = Value.CreateBatchOfSequences(inputFeatures.Shape, UtilTensor.CreateBatchSequenceArray(pos, posEnd, xTest, indices), device);//get_tensors(inputFeautures.Shape, xTrain, indices, pos, posEnd, device);
                var yValues =  Value.CreateBatchOfSequences(outputLabels.Shape, UtilTensor.CreateBatchSequenceArray(pos, posEnd, yTest, indices), device);//get_tensors(outputLabels.Shape, yTrain, indices, pos, posEnd, device);
                var dictionaryValue = new UnorderedMapVariableValuePtr()
                {
                    {inputFeatures,xValues },
                    {outputLabels,yValues }
                };

                var eval = evaluator.TestMinibatch(dictionaryValue, device);
                pos = posEnd;


                epochAccuracy += eval;

                //Console.WriteLine($"Eval= {eval}");

            }
            return epochAccuracy;
        }

        public void LoadHousePriceData(out float[][] xTrain, out float[][] yTrain, out float[][] xTest, out float[][] yTest)
        {
            var destinationFolder = "HousePrice";
            if (!System.IO.File.Exists($@"{destinationFolder}\x_train.bin"))
            {
                System.IO.Compression.ZipFile.ExtractToDirectory(@"..\..\..\house_prices.zip", $@".\{destinationFolder}");
            }
            xTrain = LoadImageDataService.LoadBinaryFile($@"{destinationFolder}\x_train.bin", 404, 13);
            yTrain = LoadImageDataService.LoadBinaryFile($@"{destinationFolder}\y_train.bin", 404, 1);
            xTest = LoadImageDataService.LoadBinaryFile($@"{destinationFolder}\x_test.bin", 102, 13);
            yTest = LoadImageDataService.LoadBinaryFile($@"{destinationFolder}\y_test.bin", 102, 1);

            Console.WriteLine("Done with loading data\n");
        }
    }
}
