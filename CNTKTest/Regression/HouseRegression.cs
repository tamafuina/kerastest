﻿using CNTKTest.Helpers;
using Statistics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CNTKTest.Regression
{
    public class HouseRegression
    {
        public void HouseData(out float[][]xTrain, out float[][]yTrain, out float[][]xTest, out float[][]yTest)
        {
            var data = LoadData();
            data = DatasetFilter.RemoveNaN(data);
            data = RemoveZeroPrice(data);
            data = RemoveConstantvariables(data);
            var (training, testing) = SplitData(data);
            SeparateFeaturesAndlabelsData(training, testing, out xTrain, out yTrain, out xTest, out yTest);
        }
        private double[,] LoadData()
        {
            return LoadDataService.ReadCsvToMatrix(@"D:\Velompanahy\prog\Datasets\HouseData\newDigitalOutput1.csv",true);
        }

        private (double[,] training,double[,] testing) SplitData(double[,] data)
        {
            DatasetManipulator.SplitDataset(data, out var trainingData, out var testingData);
            return (trainingData, testingData);
        }

        private void SeparateFeaturesAndlabelsData(double[,]trainingData, double[,] testingData, out float[][]trainingFeatures, out float[][] trainingLabels, out float[][] testingFeatures, out float[][] testingLabels)
        {
            trainingFeatures = new float[trainingData.GetLength(0)][];
            trainingLabels = new float[trainingData.GetLength(0)][];
            testingFeatures = new float[testingData.GetLength(0)][];
            testingLabels = new float[testingData.GetLength(0)][];
            for (var i = 0; i < trainingData.GetLength(0); i++)
            {
                var array = new float[trainingData.GetLength(1) - 1];
                for (var j = 0; j < trainingData.GetLength(1); j++)
                {
                    if (j == 0)
                        trainingLabels[i] = new[] { (float)trainingData[i, j] };
                    else
                    {
                        array[j - 1] = (float)trainingData[i, j];
                        trainingFeatures[i] = array;
                    }
                }
            }
            for (var i = 0; i < testingData.GetLength(0); i++)
            {
                var array = new float[testingData.GetLength(1) - 1];
                for (var j = 0; j < testingData.GetLength(1); j++)
                {
                    if (j == 0)
                        testingLabels[i] = new[] { (float)testingData[i, j] };
                    else
                    {
                       
                        array[j - 1] = (float)testingData[i, j];
                        testingFeatures[i] = array;
                    }
                }
            }
            
        }

        private static double[,] RemoveConstantvariables(double[,]data)
        {
            var std = Statistics.Statistics.StandardDeviation(data);
            var constVariable = std.Count(d => d == 0);
            if (constVariable == 0) return data;
            var newData = new double[data.GetLength(0), data.GetLength(1) - constVariable];
            var index = 0;

            for(var j=0;j<std.Length;j++)
            {
                if (std[j] == 0) continue;
                for (var i = 0; i < data.GetLength(0); i++)
                    newData[i, index] = data[i, j];
                index++;

            }
            return newData;
        }

        private static double[,] RemoveZeroPrice(double[,] data)
        {
            var indexes = new List<int>();
            for (var i = 0; i < data.GetLength(0); i++)
            {
                if (data[i, 0] == 0)
                    indexes.Add(i);
            }
            var newData = new double[data.GetLength(0) - indexes.Count, data.GetLength(1)];
            var index = 0;
            for (var i = 0; i < data.GetLength(0); i++)
            {
                if (indexes.BinarySearch(i) >= 0) continue;
                for (var j = 0; j < data.GetLength(1); j++)
                    newData[index, j] = data[i, j];
                index++;
            }
            return newData;
        }
    }
}
