﻿using Accord.Math;
using CNTK;
using CNTKTest.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CNTKTest.Regression
{
    public  class LogisticRegression
    {
        public void Train()
        {
            var device = DeviceDescriptor.GPUDevice(0);
            var input = CreateData();
            var targets = CreateTarget(100, 0).Concatenate(CreateTarget(100, 1)).Select(d=> (double)d);

            var variable = new Variable();
            var cntkFeatures = Variable.InputVariable(new NDShape(1, input.GetLength(1)), DataType.Double);
            var cntkTargets = Variable.InputVariable(new NDShape(1, 1), DataType.Double);

            var classifier = CreateLinearModel(cntkFeatures, 1, device);

            var z = CNTKLib.Sigmoid(classifier);
            var classifier2 = CreateLinearModel(z, 1, device);
            var py = CNTKLib.Sigmoid(classifier2);
            var cost = CNTKLib.ReduceMean(CNTKLib.Square(CNTKLib.Minus(py, cntkTargets)), Axis.AllAxes());

            var correctPredict = CNTKLib.Equal(CNTKLib.Round(cost), cntkTargets);
            var accuracy = CNTKLib.ReduceMean(CNTKLib.Cast(correctPredict, DataType.Double), Axis.AllAxes());

            var learningRate = new TrainingParameterScheduleDouble(0.1,1);
            var optimizer = Learner.SGDLearner(classifier2.Parameters(), learningRate);
            var trainer = Trainer.CreateTrainer(classifier2, cost, accuracy, new[] { optimizer });

            //var model =NeuralNetworkHelpers.CreateFFNeuralNetwork(cntkFeatures, 2, 1, 1, NNActivation.None, "test", device);
            //var py = CNTKLib.Sigmoid(model);
            //var cost = CNTKLib.ReduceMean(CNTKLib.Square(CNTKLib.Minus(py, cntkTargets)), Axis.AllAxes());

            //var correctPredict = CNTKLib.Equal(CNTKLib.Round(cost), cntkTargets);
            //var accuracy = CNTKLib.ReduceMean(CNTKLib.Cast(correctPredict, DataType.Double), Axis.AllAxes());

            //var learningRate = new TrainingParameterScheduleDouble(0.01);
            //var optimizer = Learner.SGDLearner(model.Parameters(), learningRate);
            //var trainer = Trainer.CreateTrainer(model, cost, accuracy, new[] { optimizer });

            for (var epoch=0;epoch<1000;epoch++)
            {
                var dictionary = new Dictionary<Variable, Value>
                {
                    { cntkFeatures, Value.CreateBatch(new[]{ input.GetLength(1)},ConvertArraryToEnumerableArray(input),device)},
                    { cntkTargets, Value.CreateBatch(new[]{1 },targets,device) }
                };
                trainer.TrainMinibatch(dictionary, true, device);
                var misMatches = trainer.PreviousMinibatchLossAverage();

                //var inputAccuracy = new Dictionary<Variable, Value>
                //{
                //   { cntkFeatures, Value.CreateBatch(new[]{ input.GetLength(1)},ConvertArraryToEnumerableArray(input),device)}
                //};
                //var outputAccuracy = new Dictionary<Variable, Value>
                //{
                //    { cntkTargets, null }
                //};
                //accuracy.Evaluate(inputAccuracy, outputAccuracy, device);
                //var outputData = outputAccuracy[cntkTargets].GetDenseData<double>(cntkTargets);

                //var actualLabels = outputData.Select(l => l.IndexOf(l.Max())).ToList();
                //var indexTargets = targets.Select(d => (int)Int16.Parse(d.ToString())).ToList();
                //int misMatches = SequenceEqual(indexTargets,actualLabels);
                Console.WriteLine($"Acccuracy: {1-misMatches}");
            }
        }

        private int SequenceEqual<T>(IList<T> expected,IList<T>predicited)
        {
            var sum = 0;
            for (var index = 0; index < expected.Count(); index++)
                sum += expected[index].Equals(predicited[index]) ? 0 : 1;
            return sum;
        }

        private static Function CreateLinearModel(Variable input,int outputDim,DeviceDescriptor device)
        {
            var weights = new Parameter( new[] { outputDim , input.Shape[0] }, DataType.Double, 1, device, "w");
            var bias = new Parameter(new[] { outputDim }, DataType.Double, 0, device, "b");
            return CNTKLib.Times(input, weights) + bias;
        }

        private T[]ConvertArraryToEnumerableArray<T>(T[,] data)
        {
            var sample = data.GetLength(0);
            var dim = data.GetLength(1);
            var values = new T[sample*dim];
            for(var index=0;index<data.GetLength(1);index++)
            {
                for (var rowIndex = 0; rowIndex < data.GetLength(0); rowIndex++)
                    values[index * dim + rowIndex] = data[rowIndex, index];
                
            }
            return values;
        }

        private float[,] CreatePatient(int rows, int cols, double[,] constant)
        {
            if (constant == null) throw new ArgumentNullException(nameof(constant));
            if (constant.GetLength(1) != cols) throw new ArgumentOutOfRangeException();

            var patient = new float[rows, cols];
            var rd = new Random(1);
            for (var rowIndex = 0; rowIndex < rows; rowIndex++)
            {
                for (var colIndex = 0; colIndex < cols; colIndex++)
                    patient[rowIndex, colIndex] =float.Parse( (rd.NextDouble() + constant[0, colIndex]).ToString());
            }

            return patient;
        }

        private int[] CreateTarget(int rows, int value)
        {
            var target = new int[rows];
            for (var index = 0; index < rows; index++)
                target[index] = value;
            return target;
        }

        private double[,] CreateData()
        {
            var size = 100;
            var random = new Random(1);
            var sick = CreatePatient(size, 2, new double[,] { { 2, 2 } });
            // var sick2 = new int[100, 2];
            var healthy = CreatePatient(size, 2, new double[,] { { -2, -2 } });
            //var healthy2 = new int[100, 2];


            var patients = new double[sick.GetLength(0) + healthy.GetLength(0), 2];
            for (var index = 0; index < size; index++)
            {
                patients[index, 0] = sick[index, 0];
                patients[index, 1] = sick[index, 1];
                patients[index + size, 0] = healthy[index, 0];
                patients[index + size, 1] = healthy[index, 1];
                //patients[index + (size * 2), 0] = healthy[index, 0];
                //patients[index + (size * 2), 1] = healthy[index, 1];
                //patients[index + (size * 3), 0] = healthy[index, 0];
                //patients[index + (size * 3), 1] = healthy[index, 1];
            }
            return patients;
        }

    }
}
