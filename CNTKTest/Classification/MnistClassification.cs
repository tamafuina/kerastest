﻿using CNTK;
using CNTKTest.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;

using feed_t = System.Collections.Generic.Dictionary<CNTK.Variable, CNTK.Value>;

namespace CNTKTest.Classification
{
    public class MnistClassification
    {

        public void Train()
        {
            var device = DeviceDescriptor.GPUDevice(0);
            var batchSize = 128;

            LoadData(out var trainImages, out var testImages, out var trainLabels, out var testLabels);

            CreateNetwork(device, out var imageTensor, out var labelTensor, out var lossFunction, out var accuracyFunction, out var trainer);
            TrainNetwork(imageTensor, labelTensor, trainImages, trainLabels, device, trainer, batchSize);
            EvaluateNetwork(batchSize, accuracyFunction, imageTensor, labelTensor, testImages, testLabels, device);


            //CNTK.Function network;
            //CNTK.Function loss_function;
            //CNTK.Function accuracy_function;
            //CNTK.Trainer trainer;

            //CNTK.Variable image_tensor;
            //CNTK.Variable label_tensor;
            //CNTK.DeviceDescriptor computeDevice;

            //float[][] train_images;
            //float[][] test_images;
            //float[][] train_labels;
            //float[][] test_labels;

            //if (!System.IO.File.Exists("train_images.bin"))
            //{
            //    System.IO.Compression.ZipFile.ExtractToDirectory("mnist_data.zip", ".");
            //}
            //train_images = LoadImageDataService.LoadBinaryFile("train_images.bin", 60000, 28 * 28);
            //test_images = LoadImageDataService.LoadBinaryFile("test_images.bin", 10000, 28 * 28);
            //train_labels = LoadImageDataService.LoadBinaryFile("train_labels.bin", 60000, 10);
            //test_labels = LoadImageDataService.LoadBinaryFile("test_labels.bin", 60000, 10);
            //Console.WriteLine("Done with loading data\n");

            //computeDevice = DeviceDescriptor.GPUDevice(0);
            //Console.WriteLine("Compute Device: " + computeDevice.AsString());

            //image_tensor = CNTK.Variable.InputVariable(CNTK.NDShape.CreateNDShape(new int[] { 28, 28 }), CNTK.DataType.Float);
            //label_tensor = CNTK.Variable.InputVariable(CNTK.NDShape.CreateNDShape(new int[] { 10 }), CNTK.DataType.Float);

            //network = Util.Dense(image_tensor, 512, computeDevice);
            //network = CNTK.CNTKLib.ReLU(network);
            //network = Util.Dense(network, 10, computeDevice);

            //loss_function = CNTK.CNTKLib.CrossEntropyWithSoftmax(network.Output, label_tensor);
            //accuracy_function = CNTK.CNTKLib.ClassificationError(network.Output, label_tensor);

            //var parameterVector = new CNTK.ParameterVector((System.Collections.ICollection)network.Parameters());
            //var learner = CNTK.CNTKLib.RMSPropLearner(parameterVector, new CNTK.TrainingParameterScheduleDouble(0.99), 0.95, 2.0, 0.5, 2.0, 0.5);
            //trainer = CNTK.CNTKLib.CreateTrainer(network, loss_function, accuracy_function, new CNTK.LearnerVector() { learner });

            //int epochs = 5;
            //int batch_size = 128;

            //for (int current_epoch = 0; current_epoch < epochs; current_epoch++)
            //{
            //    Console.WriteLine(string.Format("Epoch {0}/{1}", current_epoch + 1, epochs));
            //    var train_indices = Util.ShuffledIndices(60000);
            //    var position = 0;
            //    while (position < train_indices.Length)
            //    {
            //        var pos_end = Math.Min(position + batch_size, train_indices.Length);
            //        var minibatch_images = Util.get_tensors(image_tensor.Shape, train_images, train_indices, position, pos_end, computeDevice);
            //        var minibatch_labels = Util.get_tensors(label_tensor.Shape, train_labels, train_indices, position, pos_end, computeDevice);
            //        var feed_dictionary = new feed_t() { { image_tensor, minibatch_images }, { label_tensor, minibatch_labels } };
            //        trainer.TrainMinibatch(feed_dictionary, false, computeDevice);
            //        if (position % 100 == 0)
            //        {
            //            Console.WriteLine($"Loss function {trainer.PreviousMinibatchLossAverage()}");
            //            Console.WriteLine($"Average function {trainer.PreviousMinibatchEvaluationAverage()}");
            //        }
            //        position = pos_end;
            //    }
            //}


            //var pos = 0;
            //var accuracy = 0.0;
            //var num_batches = 0;
            //var evaluator = CNTK.CNTKLib.CreateEvaluator(accuracy_function);
            //while (pos < test_images.Length)
            //{
            //    //var pos_end = Math.Min(pos + batch_size, test_images.Length);
            //    //var minibatch_images = Util.get_tensors(image_tensor.Shape, test_images, pos, pos_end, computeDevice);
            //    //var minibatch_labels = Util.get_tensors(label_tensor.Shape, test_labels, pos, pos_end, computeDevice);
            //    //var feed_dictionary = new CNTK.UnorderedMapVariableValuePtr() { { image_tensor, minibatch_images }, { label_tensor, minibatch_labels } };
            //    //var minibatch_accuracy = evaluator.TestMinibatch(feed_dictionary, computeDevice);
            //    //accuracy += minibatch_accuracy;
            //    //pos = pos_end;
            //    //num_batches++;
            //}
            //accuracy /= num_batches;
            //Console.WriteLine(string.Format("Accuracy:{0:F3}", accuracy));
        }
        public void CreateNetwork(DeviceDescriptor device, out Variable imageTensor, out Variable labelTensor, out Function lossFunction, out Function accuracyFunction, out Trainer trainer)
        {
            
            imageTensor = Variable.InputVariable(NDShape.CreateNDShape(new[] { 28, 28 }), DataType.Float);
            labelTensor = Variable.InputVariable(NDShape.CreateNDShape(new[] { 10 }), DataType.Float);

            var network = Util.Dense(imageTensor, 512, device);
            network = CNTKLib.ReLU(network);
            network = Util.Dense(network, 10, device);

            lossFunction = CNTKLib.CrossEntropyWithSoftmax(network.Output, labelTensor);
            accuracyFunction = CNTKLib.ClassificationError(network.Output, labelTensor);

            var parameterVector = new ParameterVector((System.Collections.ICollection)network.Parameters());
            //var learner = CNTKLib.RMSPropLearner(parameterVector, new TrainingParameterScheduleDouble(0.99), 0.95, 2.0, 0.5, 2.0, 0.5);
            var learner = CNTKLib.SGDLearner(parameterVector, new TrainingParameterScheduleDouble(0.99));
            trainer = CNTKLib.CreateTrainer(network, lossFunction, accuracyFunction, new LearnerVector() { learner });
           
        }

        private static void LoadData(out float[][] trainImages, out float[][] testImages, out float[][] trainLabels, out float[][] testLabels)
        {
            if (!System.IO.File.Exists("train_images.bin"))
            {
                System.IO.Compression.ZipFile.ExtractToDirectory(@"D:\Velompanahy\prog\KerasTest\mnist_data.zip", ".");
            }
            trainImages = LoadImageDataService.LoadBinaryFile("train_images.bin", 60000, 28 * 28);
            testImages = LoadImageDataService.LoadBinaryFile("test_images.bin", 10000, 28 * 28);
            trainLabels = LoadImageDataService.LoadBinaryFile("train_labels.bin", 60000, 10);
            testLabels = LoadImageDataService.LoadBinaryFile("test_labels.bin", 10000, 10);
        }

        private void TrainNetwork(Variable featuresTensor,Variable labelsTensor, float[][]trainData, float[][]labelData,DeviceDescriptor device, Trainer trainer, int batchSize)
        {
            var epochs = 5;
            

            for(var currentEpoch=0; currentEpoch<epochs;currentEpoch++)
            {
                var trainIndices = Util.ShuffledIndices(60000);
                var position = 0;
                while (position < trainIndices.Length)
                {
                    var endPosition = Math.Min(position + batchSize, trainIndices.Length);
                    var miniBatchImages = Util.GetTensors(featuresTensor.Shape, trainData,trainIndices, position, endPosition,device);
                    var miniBatchLabels = Util.GetTensors(labelsTensor.Shape, labelData, trainIndices, position, endPosition, device);

                    var feedDictionary = new Dictionary<Variable, Value> { { featuresTensor, miniBatchImages }, { labelsTensor, miniBatchLabels } };
                    trainer.TrainMinibatch(feedDictionary, false, device);
                    
                    position = endPosition;
                }
                if (position % 100 == 0)
                {
                    Console.WriteLine($"Loss function {trainer.PreviousMinibatchLossAverage()}");
                    Console.WriteLine($"Average function {trainer.PreviousMinibatchEvaluationAverage()}");
                }

            }
        }

        private void EvaluateNetwork(int batchSize, Function accuracyFunction, Variable featuresTensor, Variable labelsTensor, float[][]featuresTestData, float[][]labelsTestData,DeviceDescriptor device)
        {
            var position = 0;
            var accuracy = 0.0;
            var numberBatch = 0;
            var evaulator = CNTKLib.CreateEvaluator(accuracyFunction);
            while(position<featuresTestData.Length)
            {
                var endPosition = Math.Min(position + batchSize, featuresTestData.Length);
                var miniBatchFeaturesTest = Util.GetTensors(featuresTensor.Shape, featuresTestData, position, endPosition, device);
                var miniBatchLabelsTest = Util.GetTensors(labelsTensor.Shape, labelsTestData, position, endPosition, device);
                var feeddDictionary = new UnorderedMapVariableValuePtr() { { featuresTensor, miniBatchFeaturesTest }, { labelsTensor, miniBatchLabelsTest } };
                var minibatchAccuracy = evaulator.TestMinibatch(feeddDictionary, device);
                accuracy += minibatchAccuracy;
                position = endPosition;
                //Console.WriteLine(string.Format("Accuracy:{0:F3}", accuracy / numberBatch));
                numberBatch++;
                
            }
            accuracy /= numberBatch;
            Console.WriteLine(string.Format("Accuracy:{0:F3}", accuracy));
        }

        private void PredictData(Function network, Variable input, float[][]testingImage, float[][]labelTestdata, DeviceDescriptor device)
        {
            var inputDataMap = new Dictionary<Variable, Value> {
                { input,Util.GetTensors(input.Shape, testingImage, 0, testingImage.Length,device)

                } };
            var outputDataMap = new Dictionary<Variable, Value>() {
            { network.Output, null } };
            network.Evaluate(inputDataMap, outputDataMap, device);
            var prediction = outputDataMap[network.Output].GetDenseData<float>(network.Output);
            
            var accuracy = 0d;
            for (var i = 0; i < prediction.Count; i++)
            {
                var indexPredict = prediction[i].IndexOf(prediction[i].Max());
                var array = labelTestdata[i].ToList();
                var indexLabel = array.IndexOf(array.Max());
                accuracy += indexPredict == indexLabel ? 1 : 0;
            }
            accuracy /= (prediction.Count);
            Console.WriteLine($"Accuracy prediction {accuracy}");

        }

        public void TrainFromTensorflow()
        {
            var device = DeviceDescriptor.GPUDevice(0);
            var batchSize = 128;

            LoadData(out var trainImages, out var testImages, out var trainLabels, out var testLabels);

            var network = CreateNetworkTensor(device, out var imageTensor, out var labelTensor, out var lossFunction, out var accuracyFunction, out var trainer);
            TrainNetwork(imageTensor, labelTensor, trainImages, trainLabels, device, trainer, batchSize);
            EvaluateNetwork(batchSize, accuracyFunction, imageTensor, labelTensor, testImages, testLabels, device);
            PredictData(network, imageTensor, testImages, testLabels, device);
        }

        private Function CreateNetworkTensor(DeviceDescriptor device, out Variable imageTensor, out Variable labelTensor, out Function lossFunction, out Function accuracyFunction, out Trainer trainer)
        {
            imageTensor = Variable.InputVariable(NDShape.CreateNDShape(new[] { 28, 28 }), DataType.Float);
            labelTensor = Variable.InputVariable(NDShape.CreateNDShape(new[] { 10 }), DataType.Float);
            var l = 200;
            var m = 100;
            var n = 60;
            var o = 30;

            var xx = CNTKLib.Reshape(imageTensor, NDShape.CreateNDShape(new[] {784, 1 }));
            var y1 = CNTKLib.Sigmoid(Util.Dense(xx, l, device, "y1"));
            var y2 = CNTKLib.Sigmoid(Util.Dense(y1, m, device, "y2"));
            var y3 = CNTKLib.Sigmoid(Util.Dense(y2, n, device, "y3"));
            var y4 = CNTKLib.Sigmoid(Util.Dense(y3, o, device, "y4"));
            var yLogit = Util.Dense(y4, 10, device, "yLogit");
            var y = CNTKLib.Softmax(yLogit);
            var loss = CNTKLib.CrossEntropyWithSoftmax(yLogit, labelTensor);
            var constant = Constant.Scalar(100f, device);
            lossFunction = CNTKLib.ElementTimes(CNTKLib.ReduceMean(loss.Output, new Axis(0)), constant);
            accuracyFunction = CNTKLib.Equal(CNTKLib.Argmax(y, new Axis(0)), CNTKLib.Argmax(labelTensor, new Axis(0)));
            var parameterVector = new ParameterVector((System.Collections.ICollection)yLogit.Parameters());
            var learner = CNTKLib.AdamLearner(parameterVector, new TrainingParameterScheduleDouble(0.003),new TrainingParameterScheduleDouble(0.99));
            trainer = CNTKLib.CreateTrainer(yLogit, lossFunction, accuracyFunction, new LearnerVector { learner });
            return yLogit;

        }
    }
}
