﻿using CNTK;
using CNTKTest.NeuralNetworkModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;

namespace CNTKTest.Classification
{
    public class IrisClassification
    {
        ObservableCollection<double> _LossData;
        ObservableCollection<double> _EvaluationAverageData;
        public void Train()
        {
            var dataFolder = "";//@"..\..\..\..\CNTK_CSharp_Minibatchsource\Data";
            var dataPath = Path.Combine(dataFolder, "test.txt");
            //var trainPath = Path.Combine(dataFolder, "iris_with_hot_vector_test.csv");

            var featureStreamName = "features";
            var labelsStreamName = "label";

            //Network definition
            int inputDim = 5;
            int numOutputClasses = 2;
            int numHiddenLayers = 1;
            int hidenLayerDim = 6;
            uint sampleSize = 130;
            var device = DeviceDescriptor.GPUDevice(0);

            //stream configuration to distinct features and labels in the file
            var streamConfig = new StreamConfiguration[]
                {
            new StreamConfiguration(featureStreamName, inputDim),
            new StreamConfiguration(labelsStreamName, numOutputClasses)
                };
            
            // build a NN model
            //define input and output variable and connecting to the stream configuration
            var feature = Variable.InputVariable(new NDShape(1, inputDim), DataType.Float, featureStreamName);
            var label = Variable.InputVariable(new NDShape(1, numOutputClasses), DataType.Float, labelsStreamName);

            var fastFeedNn = new FastFeedNeuralNetwork();
            var trainer =fastFeedNn.CreateTrainingNeuralNetwork(feature, label, NNActivation.Tanh, numHiddenLayers, hidenLayerDim, numOutputClasses, "irisModel", device, (LossErrorType.CrossEntropyAndClassification, "lossFunction", "errorFunction"));

            // prepare the training data
            var minibatchSource = MinibatchSource.TextFormatMinibatchSource(
                dataPath, streamConfig, MinibatchSource.InfinitelyRepeat, true);
            var featureStreamInfo = minibatchSource.StreamInfo(featureStreamName);
            var labelStreamInfo = minibatchSource.StreamInfo(labelsStreamName);


            //Preparation for the iterative learning process
            //used 800 epochs/iterations. Batch size will be the same as sample size since the data set is small
            int epochs = 800;
            int i = 0;
            while (epochs > -1)
            {
                var minibatchData = minibatchSource.GetNextMinibatch(sampleSize, device);
                //pass to the trainer the current batch separated by the features and label.
                var arguments = new Dictionary<Variable, MinibatchData>
                {
                    { feature, minibatchData[featureStreamInfo] },
                    { label, minibatchData[labelStreamInfo] }
                };

                trainer.TrainMinibatch(arguments, device);

                PrintTrainingProgress(trainer, i++, 50);

                // MinibatchSource is created with MinibatchSource.InfinitelyRepeat.
                // Batching will not end. Each time minibatchSource completes an sweep (epoch),
                // the last minibatch data will be marked as end of a sweep. We use this flag
                // to count number of epochs.
                if (minibatchData.Values.Any(a => a.sweepEnd))
                {
                    epochs--;
                }
            }
        }
        
        private  void PrintTrainingProgress(Trainer trainer, int minibatchIdx, int outputFrequencyInMinibatches)
        {
            if ((minibatchIdx % outputFrequencyInMinibatches) == 0 && trainer.PreviousMinibatchSampleCount() != 0)
            {
                _LossData.Add(trainer.PreviousMinibatchLossAverage());
                _EvaluationAverageData.Add(trainer.PreviousMinibatchEvaluationAverage())
                    ;
                float trainLossValue = (float)trainer.PreviousMinibatchLossAverage();
                float evaluationValue = (float)trainer.PreviousMinibatchEvaluationAverage();
                Console.WriteLine($"Minibatch: {minibatchIdx} CrossEntropyLoss = {trainLossValue}, EvaluationCriterion = {evaluationValue}");
            }
        }

        public void Train(ObservableCollection<double> lossData, ObservableCollection<double>evaluationAverageData)
        {
            _LossData = lossData ?? throw new ArgumentNullException(nameof(lossData));
            _EvaluationAverageData = evaluationAverageData ?? throw new ArgumentNullException(nameof(evaluationAverageData));
            Train();
        }
    }

}
