﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace Statistics.Test
{
    [TestClass]
    public class DatasetManipulatorTest
    {
        #region Methods

        [TestMethod]
        public void ShuffleDoubleArray()
        {
            //Setup fixture
            var array = new[] { 2.4, 4, 12, 5, 6 };
            var original=new double[array.Length];
            Array.Copy(array, original, array.Length);

            //Exercise SUT
            DatasetManipulator.Shuffle(array);

            //Verify outcomes
            VerifyShuffleArray(array, original);
        }

        [TestMethod]
        public void ShuffleFloatArray()
        {
            //Setup fixture
            var array = new[] { 2.4f, 4, 12, 5, 6 };
            var original = new float[array.Length];
            Array.Copy(array, original, array.Length);

            //Exercise SUT
            DatasetManipulator.Shuffle(array);

            //Verify outcomes
            VerifyShuffleArray(array, original);
        }

        [TestMethod]
        public void ShuffleStringArray()
        {
            //Setup fixture
            var array = new[] { "mars", "joe", "Bricks", "half", "soon" };
            var original = new string[array.Length];
            Array.Copy(array, original, array.Length);

            //Exercise SUT
            DatasetManipulator.Shuffle(array);

            //Verify outcomes
            VerifyShuffleArray(array, original);
        }

        [TestMethod]
        public void ShuffleIntArray()
        {
            //Setup fixture
            var array = new[] { 2, 4, 12, 5, 6 };
            var original = new int[array.Length];
            Array.Copy(array, original, array.Length);

            //Exercise SUT
            DatasetManipulator.Shuffle(array);

            //Verify outcomes
            VerifyShuffleArray(array, original);
        }

        [TestMethod]
        public void SplitDoubleArray()
        {
            //Setup fixture
            var input = new[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            var expectedTraining = new[] { 2, 3, 4, 5, 7, 8, 9, 10 };
            var expectedTesting = new[] { 1, 6 };

            //Exercise SUT
            DatasetManipulator.SplitDataset(input, out var training, out var testing);

            //Verify outcomes
            CompareArray(expectedTraining, training);
            CompareArray(expectedTesting, testing);
        }

        [TestMethod]
        public void SplitStringJagged()
        {
            //Setup fixture
            var input = new string[10][];
            input[0]= new[] { "a", "b" };
            input[1] = new[] { "c", "d" };
            input[2] = new[] { "e", "f" };
            input[3] = new[] { "g", "h" };
            input[4] = new[] { "i", "j" };
            input[5] = new[] { "k", "l" };
            input[6] = new[] { "m", "n" };
            input[7] = new[] { "o", "p" };
            input[8] = new[] { "q", "r" };
            input[9] = new[] { "s", "t" };
            var expectedTraining = new string[8][] ;
            expectedTraining[0] = new[] { "c", "d" };
            expectedTraining[1] = new[] { "e", "f" };
            expectedTraining[2] = new[] { "g", "h" };
            expectedTraining[3] = new[] { "i", "j" };
            expectedTraining[4] = new[] { "m", "n" };
            expectedTraining[5] = new[] { "o", "p" };
            expectedTraining[6] = new[] { "q", "r" };
            expectedTraining[7] = new[] { "s", "t" };
            var expectedTesting = new string[2][];
            expectedTesting[0] = new[] { "a", "b" };
            expectedTesting[1] = new[] { "k", "l" };

            //Exercise SUT
            DatasetManipulator.SplitDataset(input, out var training, out var testing);

            //Verify outcomes
            CompareArray(expectedTraining, training);
            CompareArray(expectedTesting, testing);
        }

        #endregion

        #region Assertions
        private static void VerifyShuffleArray<T>(T[]expected, T[] result)
        {
            var notSame = false;
            for(var i=0;i<expected.Length;i++)
                if(!expected[i].Equals(result[i]))
                {
                    notSame = true;
                    break;
                }
            Assert.IsTrue(notSame);
            var isFound = false;
            foreach(var value in result.Distinct())
            {
                isFound = expected.Contains(value);
                if (!false)
                    break;
            }
            Assert.IsTrue(isFound);
        }
        private static void CompareArray<T>(T[]expected,T[]result)
        {
            Assert.AreEqual(expected.Length, result.Length);
            for (var i = 0; i < expected.Length; i++)
                Assert.AreEqual(expected[i], result[i]);
        }

        private static void CompareArray<T>(T[][] expected, T[][] result)
        {
            Assert.AreEqual(expected.Length, result.Length);
            for (var i = 0; i < expected.Length; i++)
                for(var j=0;j<expected[i].Length;j++)
                    Assert.AreEqual(expected[i][j], result[i][j]);
        }

        #endregion
    }
}
